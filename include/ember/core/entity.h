/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/object.h>

namespace ember {
    class Entity : public Object, public std::enable_shared_from_this<Entity> {
        friend class Scene;

    public:
        explicit Entity(std::string _name);

        bool is_active() const;
        bool is_active_self() const;
        bool is_active_in_hierarchy() const;

        void set_active(bool _active);
        void set_transform(transform_ptr _transform);
        void gui_inspector() override;
        void add_component(component_ptr _component);
        void send_message(Message _m);

        entity_p get_parent() const;
        scene_p get_scene() const;
        transform_p get_transform() const;

        template<typename T>
        T* get_component() const {
            for (auto &c : components_) {
                if (auto v = dynamic_cast<T*>(c.get())) {
                    return v;
                }
            }

            return nullptr;
        }

        template<typename T>
        std::vector<T*> get_components() const {
            std::vector<T*> items;

            for (auto &c : components_) {
                if (auto item = dynamic_cast<T*>(c)) {
                    items.push_back(item);
                }
            }

            return items;
        }

        template<typename T>
        std::vector<T*> get_components_in_children() const {
            std::vector<T*> items;

            for (auto &c : _get_components_in_children()) {
                if (auto item = dynamic_cast<T*>(c)) {
                    items.push_back(item);
                }
            }

            return items;
        }

        template<typename T>
        std::vector<T*> get_components_in_parent() const {
            std::vector<T*> items;

            for (auto &c : _get_components_in_parent()) {
                if (auto item = dynamic_cast<T*>(c)) {
                    items.push_back(item);
                }
            }

            return items;
        }

    private:
        std::vector<component_p> _get_components_in_children() const;
        std::vector<component_p> _get_components_in_parent() const;

        scene_ptr scene_;
        std::vector<component_ptr> components_;
        bool active_self_;
        bool active_hierarchy_;
    };

//    entity_ptr create_entity(const std::string& _name);
//    entity_ptr create_camera(const std::string& _name);
//    entity_ptr create_light(const std::string& _name, LightType _type);
//    entity_ptr create_quad(const std::string& _name);
//    entity_ptr create_3d_object(const std::string& _name, const std::string& _object);
}
