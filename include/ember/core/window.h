/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <array>
#include <memory>

#include <glm/glm.hpp>
#include <gl3w/GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>

namespace ember {
    class Window {
        friend class App;

    public:
        Window();
        ~Window();

        static glm::ivec2 get_resolution();
        static float get_aspect_ratio();

    private:
        void handle_events();
        void begin_frame();
        void end_frame();
        void init_gui();
        void draw_gui();
        void update_mouse_pos_and_buttons();
        void update_mouse_cursor();
        void set_size(const glm::ivec2& _size);

        static void cb_char(GLFWwindow*, unsigned int _c);
        static void cb_key(GLFWwindow*, int _key, int, int _action, int _mods);
        static void cb_scroll(GLFWwindow*, double _xoffset, double _yoffset);
        static void cb_mouse_button(GLFWwindow*, int _button, int _action, int /*mods*/);
        static void cb_window_resize(GLFWwindow*, int _width, int _height);
        static void cb_mouse_move(GLFWwindow*, double _x, double _y);
        static void cb_joystick_event(int _joy, int _event);

        glm::mat4 ortho_matrix_;
        glm::ivec2 resolution_;
        glm::ivec2 mouse_position_;
        float aspect_ratio_;
        bool mouse_relative_;
        GLFWwindow* window_;
        std::array<bool, 5> mouse_just_pressed_;
        std::array<GLFWcursor*, ImGuiMouseCursor_COUNT> mouse_cursors_;
        std::unique_ptr<class Shader> gui_shader_;
        GLuint gui_vao_;
        GLuint gui_vbo_;
        GLuint gui_ibo_;
        GLuint gui_font_texture_;

        static Window* ref__;
    };
}