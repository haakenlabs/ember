/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <string>
#include <sstream>
#include <vector>

namespace ember {
    class Resource {
    public:
        enum Type {
            TypeFile,
            TypePackage,
        };

        explicit Resource(const std::string& _filename);

        std::vector<char> bytes() const;
        const char* data() const;
        std::string string() const;
        std::istringstream stream() const;
        size_t size() const;
        std::string location() const;
        std::string container() const;
        std::string base() const;
        std::string dir() const;
        std::string dir_prefix() const;
        std::string ext() const;
        Type type() const;
        void reload();

        template<typename T>
        const T* data_as() const {
            return (T*)data_.data();
        }

    private:
        std::vector<char> data_;
        std::string location_;
        std::string container_;
        Type type_;
    };
}
