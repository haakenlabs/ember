/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <cstdint>
#include <memory>

namespace ember {
    typedef class Resource* resource_p;
    typedef class Entity* entity_p;
    typedef class Component* component_p;
    typedef class Renderable* renderable_p;
    typedef class Drawable* drawable_p;
    typedef class Light* light_p;
    typedef class Scene* scene_p;
    typedef class Transform* transform_p;
    typedef class Mesh* mesh_p;
    typedef class Shader* shader_p;
    typedef class Framebuffer* framebuffer_p;
    typedef class Texture* texture_p;
    typedef class Texture2D* texture_2d_p;
    typedef class TextureCubemap* texture_cubemap_p;
    typedef class Material* material_p;
    typedef class Skybox* skybox_p;
    typedef class Effect* effect_p;

    typedef std::shared_ptr<Resource> resource_ptr;
    typedef std::shared_ptr<Entity> entity_ptr;
    typedef std::shared_ptr<Component> component_ptr;
    typedef std::shared_ptr<Renderable> renderable_ptr;
    typedef std::shared_ptr<Drawable> drawable_ptr;
    typedef std::shared_ptr<Light> light_ptr;
    typedef std::shared_ptr<Scene> scene_ptr;
    typedef std::shared_ptr<Transform> transform_ptr;
    typedef std::shared_ptr<Mesh> mesh_ptr;
    typedef std::shared_ptr<Shader> shader_ptr;
    typedef std::shared_ptr<Framebuffer> framebuffer_ptr;
    typedef std::shared_ptr<Texture> texture_ptr;
    typedef std::shared_ptr<Texture2D> texture_2d_ptr;
    typedef std::shared_ptr<TextureCubemap> texture_cubemap_ptr;
    typedef std::shared_ptr<Material> material_ptr;
    typedef std::shared_ptr<Skybox> skybox_ptr;
    typedef std::shared_ptr<Effect> effect_ptr;

    typedef uint32_t id_t;

    enum EffectType {
        EffectTypeHDR,
        EffectTypeTonemap,
        EffectTypeLDR
    };

    enum LightType {
        LightTypeDirectional,
        LightTypePoint,
        LightTypeArea,
        LightTypeSpot
    };

    enum Message {
        MessageActivate,
        MessageStart,
        MessageAwake,
        MessageUpdate,
        MessageLateUpdate,
        MessageFixedUpdate,
        MessageTransform,
        MessageGUIRender,
        MessageSGUpdate,
    };

    enum RenderMode {
        RenderModeForward,
        RenderModeDeferred,
    };

    enum TextureFormat {
        TextureFormatDefaultColor,
        TextureFormatDefaultHDRColor,
        TextureFormatDefaultDepth,
        TextureFormatR8,
        TextureFormatRG8,
        TextureFormatRGB8,
        TextureFormatRGBA8,
        TextureFormatR16,
        TextureFormatRG16,
        TextureFormatRGB16,
        TextureFormatRGBA16,
        TextureFormatRGBA16UI,
        TextureFormatR32,
        TextureFormatRG32,
        TextureFormatRGB32,
        TextureFormatRGBA32,
        TextureFormatRGB32UI,
        TextureFormatRGBA32UI,
        TextureFormatDepth16,
        TextureFormatDepth24,
        TextureFormatDepth24Stencil8,
        TextureFormatStencil8,
    };
}
