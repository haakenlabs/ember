/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <memory>

#include <ember/core/window.h>
#include <ember/core/input.h>
#include <ember/core/log.h>
#include <ember/core/object.h>
#include <ember/core/time.h>
#include <ember/core/asset.h>
#include <ember/core/scene.h>
#include <ember/gfx/renderer.h>

namespace ember {
    class App {
    public:
        App();
        ~App();

        void run();

        static void quit();

    private:
        void do_gui();
        void gui_main_menu();
        void gui_window_inspector(bool* _p_open);
        void gui_window_hierarchy(bool* _p_open);
        void gui_window_assets(bool* _p_open);
        void gui_window_renderer(bool* _p_open);
        void gui_window_debug(bool* _p_open);
        void gui_window_logs(bool* _p_open);
        void gui_window_about(bool* _p_open);

        std::unique_ptr<Log> log_;
        std::unique_ptr<ObjectManager> instance_;
        std::unique_ptr<Window> window_;
        std::unique_ptr<Input> input_;
        std::unique_ptr<Time> time_;
        std::unique_ptr<AssetManager> asset_manager_;
        std::unique_ptr<Renderer> renderer_;

        std::shared_ptr<Scene> scene_;

        bool show_menu_bar_{true};
        bool show_window_inspector_{false};
        bool show_window_hierarchy_{false};
        bool show_window_assets_{false};
        bool show_window_renderer_{false};
        bool show_window_debug_{false};
        bool show_window_logs_{false};
        bool show_window_metrics_{false};
        bool show_window_demo_{false};
        bool show_window_about_{false};

        static bool running__;
    };
}
