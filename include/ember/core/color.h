/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <string>

#include <glm/glm.hpp>

namespace ember {
    class Color {

    public:
        Color(float _r, float _g, float _b);
        Color(float _r, float _g, float _b, float _a);
        explicit Color(float _v);
        explicit Color(glm::vec3 _v);
        explicit Color(glm::vec4 _v);
        explicit Color(const std::string& _str);

        void gui_inspector();

        Color gamma() const;
        Color grayscale() const;
        Color linear() const;
        float max_color_comonent() const;
        glm::vec3 as_vec3() const;
        glm::vec4 as_vec4() const;

        float& operator[](int _i);
        Color operator-(Color _b);
        Color operator+(Color _b);
        Color operator*(Color _b);
        Color operator*(float _b);
        Color operator/(float _b);

        static Color hsv_to_rgb(float _h, float _s, float _v);
        static Color hsv_to_rgb(float _h, float _s, float _v, bool _hdr);
        static Color lerp(Color _a, Color _b, float _t);
        static Color lerp_unclamped(Color _a, Color _b, float _t);
        static Color rgb_to_hsv(Color _color, float& _h, float& _s, float& _v);

        static const Color white;
        static const Color black;
        static const Color red;
        static const Color green;
        static const Color blue;

    private:
        float color_[4];
    };
}
