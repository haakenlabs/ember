/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <map>
#include <vector>

#include <ember/core/types.h>
#include <ember/gfx/renderer.h>
#include <ember/scene/light.h>

namespace ember {
    class Scene : public std::enable_shared_from_this<Scene> {
        friend class App;

    public:
        Scene();
        ~Scene();

        void load();
        void add_entity(entity_ptr _entity, id_t _parent);
        void move_entity(id_t _id, id_t _parent);
        void remove_entity(id_t _id);
        void set_entity_active(id_t _id, bool _active);
        bool has_entity(id_t _id) const;
        bool entity_has_child(id_t _id, id_t _child) const;
        entity_p entity_by_name(const std::string& _name) const;
        entity_p entity_parent(id_t _id) const;
        entity_p entity_at_node(id_t _id) const;
        std::vector<entity_p> entity_children(id_t _id) const;
        std::vector<entity_p> entity_all_children(id_t _id) const;
        std::vector<component_p> entity_components_in_parent(id_t _id) const;
        std::vector<component_p> entity_components_in_children(id_t _id) const;

    private:
        struct node {
            node(entity_ptr _entity, id_t _parent, bool _active)
                : entity_(std::move(_entity))
                , parent_(_parent)
                , active_(_active) {}

            entity_ptr entity_;
            id_t parent_;
            std::vector<id_t> children_;
            bool active_;
        };

        void update();
        void fixed_update();
        void display();
        void update_caches();
        void update_tree(node* _n, bool _active);
        void node_remove_child(node* _n, id_t _child);
        void send_message(Message _m);
        std::vector<id_t> dfs(id_t _id, bool _include_inactive);
        std::vector<entity_p> node_all_child_entities(node* _n) const;
        std::vector<entity_p> node_all_parent_entities(node* _n) const;
        node* get_node(id_t _id) const;

        std::map<id_t, std::unique_ptr<node>> nodes_;
        std::vector<entity_ptr> e_cache_;
        std::vector<component_ptr> c_cache_;
        std::vector<renderable_ptr> r_cache_;
        std::vector<drawable_ptr> d_cache_;
        std::vector<light_ptr> l_cache_;
        std::string name_;
        node *selected_node_;
        bool loaded_;
        bool started_;
        bool dirty_;

        static Scene* ref__;
    };
}