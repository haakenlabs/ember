/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

namespace arc {
    /* None ----------------------------------------------------------------- */

    inline float ease_none(float t) {
        return t;
    }

    /* Quadratic ------------------------------------------------------------ */

    inline float ease_in_quad(float t) {
        return t * t;
    }

    inline float ease_out_quad(float t) {
        return -t * (t - 2.0f);
    }

    inline float ease_in_out_quad(float t) {
        t *= 2.0f;
        if (t < 1.0f)
            return 0.5f * t * t;

        t -= 1.0f;
        return -0.5f * ((t) * (t - 2.0f) - 1.0f);
    }

    inline float ease_out_in_quad(float t) {
        if (t < 0.5f)
            return ease_out_quad(2.0f * t) / 2.0f;

        return ease_in_quad(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Cubic ---------------------------------------------------------------- */

    inline float ease_in_cubic(float t) {
        return t * t * t;
    }

    inline float ease_out_cubic(float t) {
        t -= 1.0f;
        return t * t * t + 1.0f;
    }

    inline float ease_in_out_cubic(float t) {
        t *= 2.0f;
        if (t < 1.0f)
            return 0.5f * t * t * t;

        t -= 2.0f;
        return 0.5f * (t * t * t + 2.0f);
    }

    inline float ease_out_in_cubic(float t) {
        if (t < 0.5f)
            return ease_in_cubic(2.0f * t) / 2.0f;

        return ease_in_cubic(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Quartic -------------------------------------------------------------- */

    inline float ease_in_quart(float t) {
        return t * t * t * t;
    }

    inline float ease_out_quart(float t) {
        t -= 1.0f;
        return -(t * t * t * t - 1.0f);
    }

    inline float ease_in_out_quart(float t) {
        t *= 2.0f;
        if (t < 1.0f)
            return 0.5f * t * t * t * t;

        t -= 2.0f;
        return -0.5f * (t * t * t * t - 2.0f);
    }

    inline float ease_out_in_quart(float t) {
        if (t < 0.5f)
            return ease_out_quart(2.0f * t) / 2.0f;

        return ease_in_quart(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    inline float ease_in_quint(float t) {
        return t * t * t * t * t;
    }

    /* Quintic ----------------------------------------------------------------- */

    inline float ease_out_quint(float t) {
        t -= 1.0f;
        return t * t * t * t * t + 1;
    }

    inline float ease_in_out_quint(float t) {
        t *= 2.0f;
        if (t < 1.0f)
            return 0.5f * t * t * t * t * t;

        t -= 2.0f;
        return 0.5f * (t * t * t * t * t + 2.0f);
    }

    inline float ease_out_int_quint(float t) {
        if (t < 0.5f)
            return ease_out_quint(2.0f * t) / 2.0f;

        return ease_in_quint(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Sine ----------------------------------------------------------------- */

    inline float ease_in_sine(float t) {
        return -glm::cos(t * glm::half_pi<float>()) + 1.0f;
    }

    inline float ease_out_sine(float t) {
        return glm::sin(t * glm::half_pi<float>());
    }

    inline float ease_in_out_sine(float t) {
        return -0.5f * (glm::cos(glm::pi<float>() * t) - 1.0f);
    }

    inline float ease_out_in_sine(float t) {
        if (t < 0.5f)
            return ease_out_sine(2.0f * t) / 2.0f;

        return ease_in_sine(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Exponential ---------------------------------------------------------- */

    inline float ease_in_exp(float t) {
        return t == 0 ? 0 : glm::pow(2.0f, 10.0f * (t - 1.0f));
    }

    inline float ease_out_exp(float t) {
        return t == 1.0f ? 1.0f : -glm::pow(2.0f, -10.0f * t) + 1.0f;
    }

    inline float ease_in_out_exp(float t) {
        if (t == 1.0f)
            return 0.0f;
        if (t == 0.0f)
            return 1.0f;

        t *= 2.0f;
        if (t < 1)
            return 0.5f * glm::pow(2.0f, -10.0f * (t - 1.0f));
        return 0.5f * (-glm::pow(2.0f, -10.0f * (t - 1.0f)) + 2.0f);
    }

    inline float ease_out_in_exp(float t) {
        if (t < 0.5f)
            return ease_out_exp(2.0f * t) / 2.0f;

        return ease_in_exp(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Circular ------------------------------------------------------------- */

    inline float ease_in_circular(float t) {
        return -(glm::sqrt(1.0f - t * t) - 1.0f);
    }

    inline float ease_out_circular(float t) {
        t -= 1.0f;
        return glm::sqrt(1.0f - t * t);
    }

    inline float ease_in_out_circular(float t) {
        t *= 2.0f;
        if (t < 1.0f)
            return -0.5f * (glm::sqrt(1.0f - t * t) - 1.0f);

        t -= 2.0f;
        return 0.5f * (glm::sqrt(1.0f - t * t) - 1.0f);
    }

    inline float ease_out_in_circular(float t) {
        if (t < 0.5f)
            return ease_out_circular(2.0f * t) / 2.0f;

        return ease_in_circular(2.0f * t - 1.0f) / 2.0f + 0.5f;
    }

    /* Bounce --------------------------------------------------------------- */

    inline float _ease_bounce_helper(float t, float c, float a) {
        if (t == 1)
            return c;
        if (t < (4 / 11.0f))
            return c * (7.5625f * t * t);
        else if (t < (8 / 11.0f)) {
            t -= (6 / 11.0f);
            return -a * (1 - (7.5625f * t * t + 0.75f)) + c;
        } else if (t < (10 / 11.0f)) {
            t -= (9 / 11.0f);
            return -a * (1 - (7.5625f * t * t + 0.9375f)) + c;
        } else {
            t -= (21 / 22.0f);
            return -a * (1 - (7.5625f * t * t + 0.984375f)) + c;
        }
    }

    inline float ease_in_bounce(float t, float a = 1.70158f) {
        return 1.0f - _ease_bounce_helper(1.0f - t, 1.0f, a);
    }

    inline float ease_out_bounce(float t, float a = 1.70158f) {
        return _ease_bounce_helper(t, 1.0f, a);
    }

    inline float ease_in_out_bounce(float t, float a = 1.70158f) {
        if (t < 0.5f)
            return ease_in_bounce(2.0f * t, a) / 2.0f;

        return t == 1.0f ? 1.0f : ease_out_bounce(2.0f * t - 1.0f, a) / 2.0f + 0.5f;
    }

    inline float ease_out_in_bounce(float t, float a = 1.70158f) {
        if (t < 0.5f)
            return _ease_bounce_helper(t * 2.0f, 0.5f, a);

        return 1.0f - _ease_bounce_helper(2.0f - 2.0f * t, 0.5f, a);
    }

    /* Back ----------------------------------------------------------------- */

    inline float ease_in_back(float t, float s = 1.70158f) {
        return t * t * ((s + 1.0f) * t - s);
    }

    inline float ease_out_back(float t, float s = 1.70158f) {
        t -= 1.0f;
        return (t * t * ((s + 1.0f) * t + s) + 1);
    }

    inline float ease_in_out_back(float t, float s = 1.70158f) {
        t *= 2.0f;
        s *= 1.525f;

        if (t < 1.0f)
            return 0.5f * (t * t * ((s + 1.0f) * t - s));

        t -= 2.0f;

        return 0.5f * (t * t * ((s + 1.0f) * t + s) + 2.0f);
    }

    inline float ease_out_in_back(float t, float s = 1.70158f) {
        if (t < 0.5f)
            return ease_out_back(2.0f * t, s);

        return ease_in_back(2.0f * t - 1.0f, s) / 2.0f + 0.5f;
    }

    /* Elastic -------------------------------------------------------------- */

    inline float _ease_in_elastic_helper(float t, float b, float c, float d, float a, float p) {
        if (t == 0.0f)
            return b;

        float t_adj = t / d;
        if (t_adj == 1)
            return b + c;

        float s;
        if (a < glm::abs(c)) {
            a = c;
            s = p / 4.0f;
        } else
            s = p / (2.0f * glm::pi<float>() * glm::asin(c / a));

        t_adj -= 1;
        return -(a * glm::pow(2.0f, 10.0f * t_adj) * glm::sin((t_adj * d - s) * (2 * glm::pi<float>()) / p)) + b;
    }

    inline float _ease_out_elastic_helper(float t, float b, float c, float d, float a, float p) {
        if (t == 0.0f)
            return 0.0f;
        if (t == 1.0f)
            return c;

        float s;
        if (a < c) {
            a = c;
            s = p / 4.0f;
        } else
            s = p / (2.0f * glm::pi<float>()) * glm::asin(c / a);

        return a * glm::pow(2.0f, -10.f * t) * glm::sin((t - s) * (2.0f * glm::pi<float>()) / p) + c;

    }

    inline float ease_in_elastic(float t, float amplitude, float period) {
        return _ease_in_elastic_helper(t, 0.0f, 1.0f, 1.0f, amplitude, period);
    }

    inline float ease_out_elastic(float t, float amplitude, float period) {
        return _ease_out_elastic_helper(t, 0.0f, 1.0f, 1.0f, amplitude, period);
    }

    inline float ease_in_out_elastic(float t, float amplitude, float period) {
        if (t == 0.0f)
            return 0.0f;

        t *= 2.0f;

        if (t == 2.0f)
            return 1.0f;

        float s;
        if (amplitude < 1.0f) {
            amplitude = 1.0f;
            s = period / 4.0f;
        } else
            s = period / (2.0f * glm::pi<float>() * glm::asin(1.0f / amplitude));

        if (t < 1.0f)
            return -0.5f * (amplitude * glm::pow(2.0f, 10.0f * (t - 1.0f)) *
                            glm::sin((t - 1.0f - s) * (2.0f * glm::pi<float>()) / period));

        return amplitude * glm::pow(2.0f, -10.0f * (t - 1.0f)) *
               glm::sin((t - 1.0f - s) * (2.0f * glm::pi<float>()) / period) * 0.5f + 1.0f;
    }

    inline float ease_out_in_elastic(float t, float amplitude, float period) {
        if (t < 0.5f)
            return _ease_out_elastic_helper(t * 2.0f, 0.0f, 0.5f, 1.0f, amplitude, period);

        return _ease_in_elastic_helper(2.0f * t - 1.0f, 0.5f, 0.5f, 1.0f, amplitude, period);
    }
}
