/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/writer.h>

namespace ember {
    template<typename T>
    struct convert;

    static std::string dump_json(const rapidjson::Document &_document, const bool &_pretty_write = true)
    {
        rapidjson::StringBuffer buffer;
        if (_pretty_write)
        {
            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
            _document.Accept(writer);
        }
        else
        {
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            _document.Accept(writer);
        }

        return std::string(buffer.GetString());
    }

    static rapidjson::Document load_json(const std::vector<char> &_data)
    {
        rapidjson::Document d;
        if (_data.back() != '\0')
        {
            auto tmp = _data;

            tmp.push_back('\0');
            d.Parse(tmp.data());
        }
        else
            d.Parse(_data.data());
        return d;
    }

    template<typename T>
    static T get_json_value(const rapidjson::Document &_document, const std::string &_key)
    {
        rapidjson::Value::ConstMemberIterator ci = _document.FindMember(_key.c_str());

        if (ci == _document.MemberEnd())
            throw std::invalid_argument("Could not get value for key \"" + _key + "\", does not exist");

        T value;

        if (!convert<T>::decode(ci, value))
            throw std::invalid_argument("Could not get value for key \"" + _key + "\", type mismatch");

        return value;
    }

    template<typename T>
    static T get_json_value(const rapidjson::Document &_document, const std::string &_key, const T &_optional)
    {
        try
        {
            return get_json_value<T>(_document, _key);
        }
        catch (const std::invalid_argument)
        {
            return _optional;
        }
    }

    template<>
    struct convert<bool>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, bool &rhs)
        {
            if (!ci->value.IsBool())
                return false;

            rhs = ci->value.GetBool();

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const bool &rhs)
        {
            mi->value.SetBool(rhs);

            return true;
        }
    };

    template<>
    struct convert<int>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, int &rhs)
        {
            if (!ci->value.IsInt())
                return false;

            rhs = ci->value.GetInt();

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const int &rhs)
        {
            mi->value.SetInt(rhs);

            return true;
        }
    };

    template<>
    struct convert<unsigned int>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, unsigned int &rhs)
        {
            if (!ci->value.IsUint())
                return false;

            rhs = ci->value.GetUint();

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const unsigned int &rhs)
        {
            mi->value.SetUint(rhs);

            return true;
        }
    };

    template<>
    struct convert<float>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, float &rhs)
        {
            if (!ci->value.IsDouble())
                return false;

            rhs = static_cast<float>(ci->value.GetDouble());

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const float &rhs)
        {
            mi->value.SetDouble(rhs);

            return true;
        }
    };

    template<>
    struct convert<double>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, double &rhs)
        {
            if (!ci->value.IsDouble())
                return false;

            rhs = ci->value.GetDouble();

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const double &rhs)
        {
            mi->value.SetDouble(rhs);

            return true;
        }
    };

    template<>
    struct convert<std::string>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, std::string &rhs)
        {
            if (!ci->value.IsString())
                return false;

            rhs = ci->value.GetString();

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, const std::string &rhs)
        {
            mi->value.SetString(rapidjson::StringRef(rhs.c_str(), rhs.length()));

            return true;
        }
    };

    template<>
    struct convert<glm::ivec2>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::ivec2 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 2)
                return false;

            if (!ci->value[0].IsInt() ||
                !ci->value[1].IsInt())
                return false;

            rhs = glm::ivec2(ci->value[0].GetInt(),
                             ci->value[1].GetInt());

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::ivec2 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);

            return true;
        }
    };

    template<>
    struct convert<glm::ivec3>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::ivec3 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 3)
                return false;

            if (!ci->value[0].IsInt() ||
                !ci->value[1].IsInt() ||
                !ci->value[2].IsInt())
                return false;

            rhs = glm::ivec3(ci->value[0].GetInt(),
                             ci->value[1].GetInt(),
                             ci->value[2].GetInt());

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::ivec3 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);
            mi->value.PushBack(rhs.z, allocator);

            return true;
        }
    };

    template<>
    struct convert<glm::ivec4>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::ivec4 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 4)
                return false;

            if (!ci->value[0].IsInt() ||
                !ci->value[1].IsInt() ||
                !ci->value[2].IsInt() ||
                !ci->value[3].IsInt())
                return false;

            rhs = glm::ivec4(ci->value[0].GetInt(),
                             ci->value[1].GetInt(),
                             ci->value[2].GetInt(),
                             ci->value[3].GetInt());

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::ivec4 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);
            mi->value.PushBack(rhs.z, allocator);
            mi->value.PushBack(rhs.w, allocator);

            return true;
        }
    };

    template<>
    struct convert<glm::vec2>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::vec2 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 2)
                return false;

            if (!ci->value[0].IsNumber() ||
                !ci->value[1].IsNumber())
                return false;

            rhs = glm::vec2(static_cast<float>(ci->value[0].GetDouble()),
                            static_cast<float>(ci->value[1].GetDouble()));

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::vec2 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);

            return true;
        }
    };

    template<>
    struct convert<glm::vec3>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::vec3 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 3)
                return false;

            if (!ci->value[0].IsNumber() ||
                !ci->value[1].IsNumber() ||
                !ci->value[2].IsNumber())
                return false;

            rhs = glm::vec3(static_cast<float>(ci->value[0].GetDouble()),
                            static_cast<float>(ci->value[1].GetDouble()),
                            static_cast<float>(ci->value[2].GetDouble()));

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::vec3 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);
            mi->value.PushBack(rhs.z, allocator);

            return true;
        }
    };

    template<>
    struct convert<glm::vec4>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, glm::vec4 &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            if (ci->value.Size() != 4)
                return false;

            if (!ci->value[0].IsNumber() ||
                !ci->value[1].IsNumber() ||
                !ci->value[2].IsNumber() ||
                !ci->value[3].IsNumber())
                return false;

            rhs = glm::vec4(static_cast<float>(ci->value[0].GetDouble()),
                            static_cast<float>(ci->value[1].GetDouble()),
                            static_cast<float>(ci->value[2].GetDouble()),
                            static_cast<float>(ci->value[3].GetDouble()));

            return true;
        }

        static bool encode(rapidjson::Value::MemberIterator &mi, rapidjson::Document &doc, const glm::vec4 &rhs)
        {
            if (mi->value.IsArray())
                mi->value.Clear();

            rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

            mi->value.PushBack(rhs.x, allocator);
            mi->value.PushBack(rhs.y, allocator);
            mi->value.PushBack(rhs.z, allocator);
            mi->value.PushBack(rhs.w, allocator);

            return true;
        }
    };

    template<>
    struct convert<std::vector<std::string>>
    {
        static bool decode(rapidjson::Value::ConstMemberIterator &ci, std::vector<std::string> &rhs)
        {
            if (!ci->value.IsArray())
                return false;

            for (rapidjson::SizeType i = 0; i < ci->value.Size(); i++)
                rhs.push_back(ci->value[i].GetString());

            return true;
        }

        static bool encode(rapidjson::Value::ConstMemberIterator &ci, const std::vector<std::string> &rhs)
        {
            return false;
        }
    };
}
