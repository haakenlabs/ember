/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <string>

static const char* BUILTIN_SHADER_RTT = "_BUILTIN_RTT_";
static const char* BUILTIN_SHADER_CUBEMAP_RTT = "_BUILTIN_CUBEMAP_RTT_";
static const char* BUILTIN_SHADER_CUBEMAP_CONV = "_BUILTIN_CUBEMAP_CONV_";

static const char* BUILTIN_MESH_CUBE = "cube";
static const char* BUILTIN_MESH_QUAD = "quad";
static const char* BUILTIN_MESH_QUAD_BACK = "quad_back";

static const char* SRC_SHADER_RTT =
    "#ifdef _VERTEX_\n"
    "layout(location = 0) in vec3 vertex;\n"
    "layout(location = 1) in vec3 normal;\n"
    "layout(location = 2) in vec2 uv;\n"
    "\n"
    "out vec2 vo_texture;\n"
    "\n"
    "void main()\n"
    "{\n"
    "    vo_texture = uv;\n"
    "\n"
    "    gl_Position = vec4(vertex, 1.0);\n"
    "}\n"
    "\n"
    "#endif\n"
    "\n"
    "#ifdef _FRAGMENT_\n"
    "in vec2 vo_texture;\n"
    "\n"
    "out vec4 fo_color;\n"
    "\n"
    "layout(binding = 0) uniform sampler2D f_attachment0;\n"
    "\n"
    "void main()\n"
    "{\n"
    "    fo_color = texture(f_attachment0, vo_texture);\n"
    "}\n"
    "#endif";

static const char* SRC_SHADER_GUI =
    "#ifdef _VERTEX_\n"
    "layout(location = 0) in vec2 position;\n"
    "layout(location = 1) in vec2 uv;\n"
    "layout(location = 2) in vec4 color;\n"
    "\n"
    "out vec2 vo_texture;\n"
    "out vec4 vo_color;\n"
    "\n"
    "uniform mat4 v_projection_matrix;\n"
    "\n"
    "void main()\n"
    "{\n"
    "\tvo_texture = uv;\n"
    "\tvo_color = color;\n"
    "\n"
    "\tgl_Position = v_projection_matrix * vec4(position, 0, 1);\n"
    "}\n"
    "\n"
    "#endif\n"
    "\n"
    "#ifdef _FRAGMENT_\n"
    "in vec2 vo_texture;\n"
    "in vec4 vo_color;\n"
    "\n"
    "out vec4 fo_color;\n"
    "\n"
    "layout(binding = 0) uniform sampler2D f_attachment0;\n"
    "\n"
    "void main()\n"
    "{\n"
    "\tfo_color = vo_color * texture(f_attachment0, vo_texture.st);\n"
    "}\n"
    "\n"
    "#endif";

static const char* SRC_SHADER_CUBEMAP_CONV =
    "#ifdef _VERTEX_\n"
    "layout(location = 0) in vec3 vertex;\n"
    "layout(location = 1) in vec3 normal;\n"
    "layout(location = 2) in vec2 uv;\n"
    "\n"
    "out vec3 vo_position;\n"
    "\n"
    "uniform mat4 v_projection_matrix;\n"
    "uniform mat4 v_view_matrix;\n"
    "\n"
    "void main()\n"
    "{\n"
    "    vo_position = vec3(v_projection_matrix * v_view_matrix * vec4(vertex, 1.0));\n"
    "\n"
    "    gl_Position = vec4(vec3(vertex.x * -1.0, vertex.yz), 1.0);\n"
    "}\n"
    "\n"
    "#endif\n"
    "\n"
    "#ifdef _FRAGMENT_\n"
    "#define M_PI 3.141592653589\n"
    "\n"
    "in vec3 vo_position;\n"
    "\n"
    "out vec4 fo_color;\n"
    "\n"
    "layout(binding = 0) uniform sampler2D f_attachment0;\n"
    "\n"
    "const vec2 invAtan = vec2(0.1591, 0.3183);\n"
    "\n"
    "vec2 SampleSphericalMap(vec3 v) {\n"
    "    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));\n"
    "    uv *= invAtan;\n"
    "    uv += 0.5;\n"
    "\n"
    "    return uv;\n"
    "}\n"
    "\n"
    "void main()\n"
    "{\n"
    "    vec2 uv = SampleSphericalMap(normalize(vo_position));\n"
    "    vec3 color = texture(f_attachment0, uv).rgb;\n"
    "\n"
    "    fo_color = vec4(color, 1.0);\n"
    "}\n"
    "\n"
    "#endif";

static const char *SRC_MODEL_CUBE =
    "v -1.000000 -1.000000 1.000000\n"
    "v -1.000000 1.000000 1.000000\n"
    "v -1.000000 -1.000000 -1.000000\n"
    "v -1.000000 1.000000 -1.000000\n"
    "v 1.000000 -1.000000 1.000000\n"
    "v 1.000000 1.000000 1.000000\n"
    "v 1.000000 -1.000000 -1.000000\n"
    "v 1.000000 1.000000 -1.000000\n"
    "vt 0.6250 0.0000\n"
    "vt 0.3750 0.2500\n"
    "vt 0.3750 0.0000\n"
    "vt 0.6250 0.2500\n"
    "vt 0.3750 0.5000\n"
    "vt 0.3750 0.2500\n"
    "vt 0.6250 0.5000\n"
    "vt 0.3750 0.7500\n"
    "vt 0.3750 0.5000\n"
    "vt 0.6250 0.7500\n"
    "vt 0.3750 1.0000\n"
    "vt 0.3750 0.7500\n"
    "vt 0.1250 0.7500\n"
    "vt 0.1250 0.5000\n"
    "vt 0.8750 0.5000\n"
    "vt 0.6250 0.2500\n"
    "vt 0.6250 0.5000\n"
    "vt 0.6250 0.7500\n"
    "vt 0.6250 1.0000\n"
    "vt 0.8750 0.7500\n"
    "vn -1.0000 0.0000 0.0000\n"
    "vn 0.0000 0.0000 -1.0000\n"
    "vn 1.0000 0.0000 0.0000\n"
    "vn 0.0000 0.0000 1.0000\n"
    "vn 0.0000 -1.0000 0.0000\n"
    "vn 0.0000 1.0000 0.0000\n"
    "s off\n"
    "f 4/1/1 1/2/1 2/3/1\n"
    "f 8/4/2 3/5/2 4/6/2\n"
    "f 6/7/3 7/8/3 8/9/3\n"
    "f 2/10/4 5/11/4 6/12/4\n"
    "f 3/5/5 5/13/5 1/14/5\n"
    "f 8/15/6 2/10/6 6/7/6\n"
    "f 4/1/1 3/16/1 1/2/1\n"
    "f 8/4/2 7/17/2 3/5/2\n"
    "f 6/7/3 5/18/3 7/8/3\n"
    "f 2/10/4 1/19/4 5/11/4\n"
    "f 3/5/5 7/8/5 5/13/5\n"
    "f 8/15/6 4/20/6 2/10/6";
