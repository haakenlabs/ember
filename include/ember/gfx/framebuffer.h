/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/object.h>

#include <map>
#include <stack>
#include <vector>

#include <glm/glm.hpp>
#include <gl3w/GL/gl3w.h>

namespace ember {
    class Framebuffer : public Object, public std::enable_shared_from_this<Framebuffer> {
        friend class Renderer;

    public:
        Framebuffer();
        ~Framebuffer() override;

        void bind();
        void unbind();
        void clear_buffers() const;
        void clear_buffer_flags(GLuint _flags) const;
        void set_size(glm::ivec2 _size);
        void attach(GLenum _location, texture_2d_p _texture, uint32_t _mip_level = 0);
        void apply_draw_buffers(const std::vector<GLenum>& _draw_buffers);

        bool validate();
        GLuint get_reference() const;
        glm::ivec2 get_size() const;

        static void blit_framebuffers(
            framebuffer_p _in,
            framebuffer_p _out,
            GLenum _location = GL_COLOR_ATTACHMENT0,
            GLenum _bit = GL_COLOR_BUFFER_BIT,
            GLenum _filter = GL_LINEAR);
        static void render_to_texture(texture_2d_p _in, texture_2d_p _out);
        static void render_cubemap_to_texture(texture_cubemap_p _in, texture_2d_p _out);

    private:
        std::vector<GLenum > draw_buffers_;
        glm::ivec2 size_;
        GLuint reference_;
        bool bound_;
    };
}