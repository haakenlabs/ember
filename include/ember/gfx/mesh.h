/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/object.h>
#include <ember/core/asset.h>

#include <gl3w/GL/gl3w.h>
#include <glm/glm.hpp>

namespace ember {
    struct Vertex {
        Vertex() {}
        Vertex(glm::vec3 _position, glm::vec3 _normal, glm::vec2 _uv)
        : position(_position)
        , normal(_normal)
        , uv(_uv) {}
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 uv;

        bool operator==(const Vertex& _other) const {
            return position == _other.position && normal == _other.normal && uv == _other.uv;
        }
    };

    class Mesh : public Object {
    public:
        enum ObjFaceType
        {
            ObjFaceTypeUnknown,
            ObjFaceTypeV,
            ObjFaceTypeVT,
            ObjFaceTypeVN,
            ObjFaceTypeVTN
        };

        Mesh();
        explicit Mesh(const std::string& _data);
        ~Mesh() override;

        void upload(const std::vector<Vertex>& _vertices);
        void upload(const std::vector<Vertex>& _vertices, const std::vector<GLuint>& _indices);
        bool load(const resource_ptr& _resource);
        bool load(const std::string& _data);
        void bind() const;
        void unbind() const;
        void draw() const;

    private:
        uint32_t vao_;
        uint32_t vbo_;
        uint32_t ibo_;
        GLuint verts_;
        GLuint indices_;
    };

    class MeshHandler : public AssetHandler<Mesh> {
    public:
        MeshHandler();

        void load(const resource_ptr& _resource) override;
        void gui_inspector() override;
    };

    mesh_ptr create_mesh_quad();
    mesh_ptr create_mesh_quad_back();
}
