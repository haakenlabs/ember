/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/types.h>

#include <vector>

namespace ember {
    class EffectProcessor {
    public:
        EffectProcessor(framebuffer_ptr _fbo, texture_2d_ptr _hdr, texture_2d_ptr _ldr);

        void add_effect(effect_ptr _effect);
        void gui_inspector();
        void effect_pass();
        void end_effect_pass();
        void process_effects();

    private:
        std::vector<effect_ptr> hdr_effects_;
        std::vector<effect_ptr> ldr_effects_;
        texture_2d_ptr _tmp_hdr;
        texture_2d_ptr _tmp_ldr;
        texture_2d_ptr hdr_;
        texture_2d_ptr ldr_;
        framebuffer_ptr fbo_;
        mesh_ptr mesh_;
        shader_ptr copy_shader_;
        effect_ptr tonemap_effect_;
        uint8_t effect_pass_;
        EffectType active_effect_type_;
    };

    class Effect {
        friend class EffectProcessor;
    public:
        explicit Effect(EffectType _type, std::string _name);

        void set_enabled(bool _enabled);

        std::string get_name() const;
        EffectType get_type() const;
        bool is_enabled() const;

        virtual void render(const EffectProcessor* _processor) = 0;
        virtual void gui_inspector() = 0;

    private:
        std::string name_;
        EffectType type_;
        bool enabled_;
    };
}