/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/object.h>
#include <ember/core/asset.h>

#include <gl3w/GL/gl3w.h>
#include <glm/glm.hpp>

namespace ember {
    GLenum format_to_storage(TextureFormat _format);
    GLenum format_to_internal(TextureFormat _format);
    GLenum format_to_glformat(TextureFormat _format);

    class Texture : virtual public Object {
    public:
        explicit Texture(GLenum _type, TextureFormat _format);
        ~Texture() override;

        void bind() const;
        void unbind() const;
        void activate(uint32_t unit) const;
        void generate_mipmaps();

        uint32_t get_type() const;
        uint32_t get_reference() const;
        GLenum get_storage() const;
        GLenum get_internal() const;
        GLenum get_glformat() const;
        GLenum get_filter_min() const;
        GLenum get_filter_mag() const;
        GLenum get_wrap_r() const;
        GLenum get_wrap_s() const;
        GLenum get_wrap_t() const;
        glm::ivec2 get_size() const;
        TextureFormat get_format() const;

        void set_wrap_r(GLenum _r);
        void set_wrap_s(GLenum _s);
        void set_wrap_t(GLenum _t);
        void set_wrap(GLenum _r, GLenum _s, GLenum _t);
        void set_filter(GLenum _filter_min, GLenum _filter_mag);
        void set_filter_min(GLenum _min);
        void set_filter_mag(GLenum _mag);
        void set_size(glm::ivec2 _size);
        void set_format(TextureFormat _format);

        virtual void upload();
        virtual void upload_data(void* _data) = 0;

    protected:
        GLenum type_;
        glm::ivec2 size_;
        GLenum reference_;
        GLenum storage_;
        GLenum internal_;
        GLenum glformat_;
        GLenum filter_min_;
        GLenum filter_mag_;
        GLenum wrap_r_;
        GLenum wrap_s_;
        GLenum wrap_t_;
        uint32_t layers_;
        TextureFormat format_;
        bool resizable_;
    };

    class Texture2D : public Texture {
    public:
        explicit Texture2D(TextureFormat _format, bool _resizable = true);
        Texture2D(TextureFormat _format, glm::ivec2 _size, bool _resizable = true);

        void upload_data(void* _data) override;
        bool load(const resource_ptr& _resource);
        void gui_inspector() override;
    };

    class TextureColor : public Texture {
    public:
        TextureColor();
        explicit TextureColor(glm::vec4 _color);

        void upload_data(void* _data) override;

        void set_color(glm::vec4 _color);
        glm::vec4 get_color() const;

    private:
        glm::vec4 color_;
    };

    class TextureCubemap : public Texture {
    public:
        explicit TextureCubemap(TextureFormat _format);
        TextureCubemap(TextureFormat _format, int32_t _size);

        void upload_data(void* _data, uint32_t _face);
        void upload_data(void* _data) override;
        bool load(const resource_ptr& _resource);
        void gui_inspector() override;
    };

    class TextureHandler : public AssetHandler<Texture2D> {
    public:
        TextureHandler();

        void load(const resource_ptr& _resource) override;
        void gui_inspector() override;
    };

    class CubemapHandler : public AssetHandler<TextureCubemap> {
    public:
        CubemapHandler();

        void load(const resource_ptr& _resource) override;
        void gui_inspector() override;
    };
}