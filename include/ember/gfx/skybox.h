/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/types.h>

namespace ember {
    class Skybox {
    public:
        explicit Skybox(texture_cubemap_ptr _source);
        Skybox(texture_cubemap_ptr _rad, texture_cubemap_ptr _irr, texture_cubemap_ptr _spec);

        const texture_cubemap_p get_radiance() const;
        const texture_cubemap_p get_irradiance() const;
        const texture_cubemap_p get_specular() const;

    protected:
        void generate_maps();

        texture_cubemap_ptr radiance_map_;
        texture_cubemap_ptr irradiance_map_;
        texture_cubemap_ptr specular_map_;
    };
}