/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <array>
#include <map>
#include <stack>
#include <vector>

#include <glm/glm.hpp>

#include <ember/core/types.h>

namespace ember {
    enum RendererBuffer {
        RendererBufferNone,
        RendererBufferPositions,
        RendererBufferNormals,
        RendererBufferDepth,
        RendererBufferAlbedo,
        RendererBufferMetallic,
        RendererBufferRoughness,
    };

    class Renderable {
    public:
        virtual float get_near_clip() const = 0;
        virtual float get_far_clip() const = 0;
        virtual glm::vec3 get_position() const = 0;
        virtual glm::mat3 get_normal_matrix() const = 0;
        virtual glm::mat4 get_view_matrix() const = 0;
        virtual glm::mat4 get_projection_matrix() const = 0;
    };

    class Drawable {
    public:
        virtual void draw(renderable_p _renderable) = 0;
        virtual void draw_shader(renderable_p _renderable, shader_p _shader) = 0;
        virtual bool is_deferrable() const = 0;
    };

    class Renderer {
    public:
        Renderer();
        ~Renderer();

        void render(
            renderable_p _renderable,
            const std::vector<drawable_ptr>& _drawables,
            const std::vector<light_ptr>& _lights);
        void set_size(glm::ivec2 _size);
        glm::ivec2 get_size() const;

        static void push_framebuffer(framebuffer_ptr _fbo);
        static void pop_framebuffer();

        static framebuffer_p get_current_framebuffer();
        static framebuffer_p get_temp_framebuffer();
        static shader_p get_builtin_shader(const std::string& _name);
        static mesh_p get_builtin_mesh(const std::string& _name);

    private:
        std::stack<framebuffer_ptr> fbo_stack_;
        std::map<std::string, shader_ptr> builtin_shaders_;
        std::map<std::string, mesh_ptr> builtin_meshes_;
        framebuffer_ptr fbo_tmp_;
        framebuffer_ptr fbo_;
        shader_ptr primary_shader_;
        shader_ptr skybox_shader_;
        shader_ptr debug_shader_;
        texture_2d_ptr tex_gbuffer_0_;
        texture_2d_ptr tex_gbuffer_1_;
        texture_2d_ptr tex_hdr_target_;
        texture_2d_ptr tex_ldr_target_;
        texture_2d_ptr tex_depth_;
        texture_2d_ptr tex_brdf_lut_;
        mesh_ptr mesh_;
        skybox_ptr skybox_;
        std::shared_ptr<class EffectProcessor> effect_processor_;
        glm::ivec2 size_;
        RenderMode active_mode_;
        RendererBuffer visualize_buffer_;

        static Renderer* ref__;
    };
}
