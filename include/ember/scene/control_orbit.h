/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/component.h>

#include <glm/glm.hpp>

namespace ember {
    class ControlOrbit : public ScriptComponent {
    public:
        ControlOrbit();

        void start() override;
        void late_update() override;
        void gui_inspector() override;

    private:
        void move();

        transform_ptr target_;

        glm::vec3 radial_;
        glm::vec3 phi_;
        glm::vec3 theta_;
        glm::vec2 mouse_start_;
        glm::vec2 mouse_last_;
        glm::vec2 mouse_delta_;
        bool mouse_drag_;
        bool mouse_down_;
    };
}
