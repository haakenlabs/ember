/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/component.h>
#include <ember/gfx/renderer.h>

namespace ember {
    class Camera : public ScriptComponent, public Renderable {
    public:
        Camera();

        void update_matrices();
        void gui_inspector() override;
        void on_transform_changed() override;

        void set_near_clip(float _near_clip);
        void set_far_clip(float _far_clip);
        void set_fov(float _fov);
        void set_view_matrix(glm::mat4 _view_matrix);
        void set_view_locked(bool _lock);
        void set_viewport(glm::ivec2 _viewport);

        float get_near_clip() const override;
        float get_far_clip() const override;
        glm::vec3 get_position() const override;
        glm::mat3 get_normal_matrix() const override;
        glm::mat4 get_view_matrix() const override;
        glm::mat4 get_projection_matrix() const override;
        glm::ivec2 get_viewport() const;

    private:
        glm::ivec2 viewport_;
        glm::mat4 view_matrix_;
        glm::mat4 projection_matrix_;
        glm::mat3 normal_matrix_;
        float fov_;
        float near_clip_;
        float far_clip_;
        bool orthographic_;
        bool locked_view_;
    };
}
