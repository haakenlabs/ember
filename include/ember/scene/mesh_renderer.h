/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <ember/core/component.h>
#include <ember/gfx/renderer.h>

namespace ember {
    class MeshRenderer : public Component, public Drawable {
    public:
        MeshRenderer(mesh_ptr _mesh, material_ptr _material);

        void set_mesh(mesh_ptr _mesh);
        void set_material(material_ptr _mesh);
        void draw(renderable_p _renderable) override;
        void draw_shader(renderable_p _renderable, shader_p _shader) override;
        void gui_inspector() override;
        bool is_deferrable() const override;

    private:
        material_ptr material_;
        mesh_ptr mesh_;
    };
}