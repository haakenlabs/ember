/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/framebuffer.h>

#include <glm/gtc/matrix_transform.hpp>

#include <ember/core/log.h>
#include <ember/core/window.h>
#include <ember/gfx/builtin.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/renderer.h>
#include <ember/gfx/shader.h>
#include <ember/gfx/texture.h>

using namespace ember;

static const glm::mat4 rotation_matrices[6] = {
    glm::lookAt(glm::vec3(0), glm::vec3(-1, 0, 0), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(1, 0, 0), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 0, 1), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, -1, 0)),
};

Framebuffer::Framebuffer() 
: bound_(false) {
    glGenFramebuffers(1, &reference_);
}

Framebuffer::~Framebuffer() {
    glDeleteFramebuffers(1, &reference_);
}

void Framebuffer::bind() {
    if (!bound_) {
        Renderer::push_framebuffer(shared_from_this());
    }
}

void Framebuffer::unbind() {
    if (bound_) {
        Renderer::pop_framebuffer();
    }
}

void Framebuffer::clear_buffers() const {
    clear_buffer_flags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Framebuffer::clear_buffer_flags(GLuint _flags) const {
    glClear(_flags);
}

void Framebuffer::set_size(glm::ivec2 _size) {
    size_ = _size;
}

void Framebuffer::attach(GLenum _location, texture_2d_p _texture, uint32_t _mip_level) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, _location, GL_TEXTURE_2D, _texture->get_reference(), _mip_level);
}

void apply_draw_buffers(const std::vector<GLenum>& _draw_buffers) {
    glDrawBuffers(int32_t(_draw_buffers.size()), _draw_buffers.data());
}

bool Framebuffer::validate() {
    uint32_t status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status == GL_FRAMEBUFFER_COMPLETE) {
        return true;
    }

    switch (status) {
        case GL_FRAMEBUFFER_UNSUPPORTED:
            LOG_ERROR("Framebuffer {} error: unsupported framebuffer format", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            LOG_ERROR("Framebuffer {} error: missing attachment", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            LOG_ERROR("Framebuffer {} error: incomplete attachment", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            LOG_ERROR("Framebuffer {} error: missing draw buffer", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            LOG_ERROR("Framebuffer {} error: missing read buffer", reference_);
            break;
        default:
            LOG_ERROR("Framebuffer {} error: unknown error: {}", reference_, status);
            break;
    }

    return false;
}

GLuint Framebuffer::get_reference() const {
    return reference_;
}

glm::ivec2 Framebuffer::get_size() const {
    return size_;
}

void Framebuffer::blit_framebuffers(
    framebuffer_p _in,
    framebuffer_p _out,
    GLenum _location,
    GLenum _bit,
    GLenum _filter
) {
    GLuint current_fbo = 0;
    if (auto f = Renderer::get_current_framebuffer()) {
        current_fbo = f->get_reference();
    }

    auto src = _in->get_reference();
    auto dst = uint32_t(0);

    auto src_size = _in->get_size();
    auto dst_size = Window::get_resolution();

    if (_out) {
        dst = _out->get_reference();
        dst_size = _out->get_size();
    }

    glBindFramebuffer(GL_READ_FRAMEBUFFER, src);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, dst);
    glReadBuffer(_location);
    glBlitFramebuffer(0, 0, src_size.x, src_size.y, 0, 0, dst_size.x, dst_size.y, _bit, _filter);

    glBindFramebuffer(GL_FRAMEBUFFER, current_fbo);
}

void Framebuffer::render_to_texture(texture_2d_p _in, texture_2d_p _out) {
    auto f = Renderer::get_temp_framebuffer();
    auto s = Renderer::get_builtin_shader(BUILTIN_SHADER_RTT);
    auto m = Renderer::get_builtin_mesh(BUILTIN_MESH_QUAD);

    f->bind();
    f->set_size(_out->get_size());

    s->bind();
    _in->activate(0);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _out->get_reference(), 0);
    f->clear_buffers();

    m->bind();
    m->draw();
    m->unbind();

    s->unbind();
    f->unbind();
}

void Framebuffer::render_cubemap_to_texture(texture_cubemap_p _in, texture_2d_p _out) {
    auto f = Renderer::get_temp_framebuffer();
    auto s = Renderer::get_builtin_shader(BUILTIN_SHADER_CUBEMAP_RTT);
    auto m = Renderer::get_builtin_mesh(BUILTIN_MESH_CUBE);

    f->bind();
    f->set_size(glm::ivec2(_in->get_size().y * 4, _in->get_size().y * 2));

    s->bind();
    _in->activate(0);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _out->get_reference(), 0);
    f->clear_buffers();

    m->bind();
    for (const auto &r : rotation_matrices) {
        s->set_uniform("u_view_matrix", r);
        m->draw();
    }

    m->unbind();

    s->unbind();
    f->unbind();
}
