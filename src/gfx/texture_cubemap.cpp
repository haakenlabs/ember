/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/texture.h>

#include <glm/gtc/matrix_transform.hpp>
#include <imgui/imgui.h>

#include <ember/core/log.h>
#include <ember/core/resource.h>
#include <ember/gfx/builtin.h>
#include <ember/gfx/framebuffer.h>
#include <ember/gfx/renderer.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/shader.h>

using namespace ember;

static const glm::mat4 rotation_matrices[6] = {
    glm::lookAt(glm::vec3(0), glm::vec3(-1, 0, 0), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(1, 0, 0), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 0, 1), glm::vec3(0, -1, 0)),
    glm::lookAt(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, -1, 0)),
};

TextureCubemap::TextureCubemap(TextureFormat _format)
: TextureCubemap(_format, 0) {}

TextureCubemap::TextureCubemap(TextureFormat _format, int32_t _size)
: Texture(GL_TEXTURE_CUBE_MAP, _format)
{
    class_name_ = "TextureCubemap";
    name_ = "TextureCubemap";
    size_ = glm::ivec2(_size);
    resizable_ = false;

    bind();
    upload();
}

void TextureCubemap::upload_data(void* _data) {
    bind();
    for (uint32_t i = 0; i < 6; i++) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, nullptr);
    }
    unbind();
}

void TextureCubemap::upload_data(void* _data, uint32_t _face) {
    bind();
    if (_face < 6) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+_face, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, _data);
    }
    unbind();
}

bool TextureCubemap::load(const resource_ptr& _resource) {
    auto source = std::make_shared<Texture2D>(TextureFormatDefaultColor);
    if (!source->load(_resource)) {
        return false;
    }

    set_name(_resource->base());
    set_format(source->get_format());
    size_ = glm::ivec2(source->get_size().y/2);

    auto fbo = Renderer::get_temp_framebuffer();
    auto mesh = Renderer::get_builtin_mesh(BUILTIN_MESH_CUBE);
    auto shader = Renderer::get_builtin_shader(BUILTIN_SHADER_CUBEMAP_CONV);

    fbo->bind();
    fbo->set_size(size_);

    mesh->bind();
    shader->bind();

    source->activate(0);
    shader->set_uniform("v_projection_matrix", glm::perspective(glm::half_pi<float>(), 1.0f, 0.1f, 2.0f));

    for (uint32_t i = 0; i < 6; i++) {
        shader->set_uniform("v_view_matrix", rotation_matrices[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, reference_, 0);
        fbo->clear_buffers();
        mesh->draw();
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);

    mesh->unbind();
    shader->unbind();
    fbo->unbind();

    return true;
}

void TextureCubemap::gui_inspector() {
    ImGui::Text("Texture type: GL_TEXTURE_CUBE_MAP");
    ImGui::Text("Reference: %u", reference_);
    ImGui::Text("Size: %d, %d", size_.x, size_.y);
}

CubemapHandler::CubemapHandler()
: AssetHandler("cubemap") {}

void CubemapHandler::load(const resource_ptr& _resource) {
    auto c = std::make_shared<TextureCubemap>(TextureFormatDefaultColor);
    if (!c->load(_resource)) {
        throw std::runtime_error("Cubemap failed to load");
    }

    items_[c->get_name()] = c;
}

void CubemapHandler::gui_inspector() {
    if (ImGui::TreeNode("Cubemap")) {
        for (auto const &c: items_) {
            if (ImGui::TreeNode(c.first.c_str())) {
                c.second->gui_inspector();
                ImGui::TreePop();
            }
        }
        ImGui::TreePop();
    }
}
