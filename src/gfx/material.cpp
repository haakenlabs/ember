/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/material.h>

#include <ember/core/log.h>
#include <ember/gfx/texture.h>

using namespace ember;

Material::Material(shader_ptr _shader)
: shader_(std::move(_shader)) {
    class_name_ = "Material";
    name_ = "Material";
}

void Material::bind() {
    shader_->bind();
    for (auto const &t : textures_) {
        t.second->activate(t.first);
    }
}

void Material::unbind() {
    shader_->unbind();
}

void Material::set_property(const std::string& _name, ShaderProperty _value) {
    if (_name.empty()) {
        LOG_ERROR("Material property name cannot be empty");
        return;
    }

    properties_[_name] = _value;
}

void Material::set_shader(shader_ptr _shader) {
    if (!_shader) {
        LOG_ERROR("Material shader cannot be null");
        return;
    }

    shader_ = std::move(_shader);
}

void Material::set_texture(texture_ptr _texture, GLuint _id) {
    if (_id >= 32) {
        LOG_ERROR("Invalid material texture ID: {}", _id);
        return;
    }
    if (!_texture) {
        LOG_ERROR("Material texture cannot be null");
        return;
    }

    textures_[_id] = std::move(_texture);
}

bool Material::supports_deferred() const {
    return shader_->supports_deferred();
}

shader_p Material::get_shader() const {
    return shader_.get();
}

void Material::gui_inspector() {
    if (ImGui::TreeNode("Shader")) {
        shader_->gui_inspector();
        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Shader Properties")) {
        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Textures")) {
        ImGui::TreePop();
    }
}
