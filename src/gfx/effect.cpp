/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/effect.h>

#include <imgui/imgui.h>

#include <ember/gfx/framebuffer.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/shader.h>
#include <ember/gfx/texture.h>

using namespace ember;

EffectProcessor::EffectProcessor(framebuffer_ptr _fbo, texture_2d_ptr _hdr, texture_2d_ptr _ldr)
: fbo_(std::move(_fbo))
, hdr_(std::move(_hdr))
, ldr_(std::move(_ldr))
, effect_pass_(0) {
    _tmp_hdr = std::make_shared<Texture2D>(TextureFormatDefaultHDRColor);
    _tmp_ldr = std::make_shared<Texture2D>(TextureFormatDefaultColor);
}

void EffectProcessor::add_effect(effect_ptr _effect) {
    switch (_effect->get_type()) {
        case EffectTypeHDR:
            hdr_effects_.push_back(std::move(_effect));
            break;
        case EffectTypeTonemap:
            tonemap_effect_ = std::move(_effect);
            break;
        case EffectTypeLDR:
            ldr_effects_.push_back(std::move(_effect));
            break;
    }
}

void EffectProcessor::gui_inspector() {
    if (ImGui::TreeNode("HDR Effects")) {
        for (auto &e : hdr_effects_) {
            ImGui::TreeNode(e->get_name().c_str());
            ImGui::Checkbox("Enabled", &e->enabled_);
            e->gui_inspector();
        }
        ImGui::TreePop();
    }
    if (ImGui::TreeNode("Tonemapper")) {
        tonemap_effect_->gui_inspector();
    }
    if (ImGui::TreeNode("LDR Effects")) {
        for (auto &e : ldr_effects_) {
            ImGui::TreeNode(e->get_name().c_str());
            ImGui::Checkbox("Enabled", &e->enabled_);
            e->gui_inspector();
        }
        ImGui::TreePop();
    }
}

void EffectProcessor::process_effects() {
    if (_tmp_hdr->get_size() != fbo_->get_size()) {
        _tmp_hdr->set_size(fbo_->get_size());
    }
    if (_tmp_ldr->get_size() != fbo_->get_size()) {
        _tmp_ldr->set_size(fbo_->get_size());
    }

    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);

    active_effect_type_ = EffectTypeHDR;

    for (auto &e : hdr_effects_) {
        if (!e->enabled_) continue;
        tonemap_effect_->render(this);
        end_effect_pass();
    }

    active_effect_type_ = EffectTypeTonemap;

    tonemap_effect_->render(this);

    active_effect_type_ = EffectTypeLDR;

    for (auto &e : ldr_effects_) {
        if (!e->enabled_) continue;
        tonemap_effect_->render(this);
        end_effect_pass();
    }

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}

void EffectProcessor::effect_pass() {
    if (active_effect_type_ == EffectTypeHDR) {
        if (effect_pass_ % 2) {
            _tmp_hdr->activate(0);
            fbo_->attach(GL_COLOR_ATTACHMENT0, hdr_.get());
        } else {
            hdr_->activate(0);
            fbo_->attach(GL_COLOR_ATTACHMENT0, _tmp_hdr.get());
        }
    } else if (active_effect_type_ == EffectTypeLDR) {
        if (effect_pass_ % 2) {
            _tmp_ldr->activate(0);
            fbo_->attach(GL_COLOR_ATTACHMENT0, ldr_.get());
        } else {
            ldr_->activate(0);
            fbo_->attach(GL_COLOR_ATTACHMENT0, _tmp_ldr.get());
        }
    }

    mesh_->bind();
    mesh_->draw();
    mesh_->unbind();

    effect_pass_++;
}

void EffectProcessor::end_effect_pass() {
    if (effect_pass_ % 2) {
        effect_pass_ = 0;
        return;
    }

    copy_shader_->bind();
    copy_shader_->set_subroutine(ShaderComponentFragment, "pass_0");

    mesh_->bind();
    mesh_->draw();
    mesh_->unbind();

    copy_shader_->unbind();

    effect_pass_ = 0;
}

Effect::Effect(EffectType _type, std::string _name)
: type_(_type)
, name_(std::move(_name)) {}

void Effect::set_enabled(bool _enabled) {
    enabled_ = _enabled;
}

std::string Effect::get_name() const {
    return name_;
}

EffectType Effect::get_type() const {
    return type_;
}

bool Effect::is_enabled() const {
    return enabled_;
}