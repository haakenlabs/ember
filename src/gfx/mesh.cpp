/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/mesh.h>

#include <chrono>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

#include <imgui/imgui.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader/tiny_obj_loader.h>

#include <ember/core/log.h>
#include <ember/core/resource.h>
#include <ember/gfx/builtin.h>

using namespace ember;

namespace std {
    template<>
    struct hash<ember::Vertex> {
        size_t operator()(ember::Vertex const &vertex) const {
            return ((hash<glm::vec3>()(vertex.position) ^ (hash<glm::vec3>()(vertex.normal) << 1)) >> 1) ^
                   (hash<glm::vec2>()(vertex.uv) << 1);
        }
    };
}

Mesh::Mesh()
: vao_(0)
, vbo_(0)
, ibo_(0)
, verts_(0)
, indices_(0) {
    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);

    glGenBuffers(1, &vbo_);
    glGenBuffers(1, &ibo_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), reinterpret_cast<void*>(sizeof(glm::vec3)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), reinterpret_cast<void*>(2 * sizeof(glm::vec3)));

    glBindVertexArray(0);
}

Mesh::Mesh(const std::string& _data)
: Mesh() {
    if (!load(_data)) {
        throw std::runtime_error("Model failed to load");
    }
}

Mesh::~Mesh() {
    glDeleteBuffers(1, &ibo_);
    glDeleteBuffers(1, &vbo_);
    glDeleteVertexArrays(1, &vao_);
}

void Mesh::upload(const std::vector<Vertex>& _vertices) {
    verts_ = (GLsizei)_vertices.size();
    indices_ = 0;

    bind();
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);
    unbind();
}

void Mesh::upload(const std::vector<Vertex>& _vertices, const std::vector<GLuint>& _indices) {
    verts_ = (GLsizei)_vertices.size();
    indices_ = (GLsizei)_indices.size();

    bind();
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(GLuint), &_indices[0], GL_STATIC_DRAW);
    unbind();
}

void Mesh::bind() const {
    glBindVertexArray(vao_);
}

void Mesh::unbind() const {
    glBindVertexArray(0);
}

void Mesh::draw() const {
    if (indices_ > 0 && verts_ > 0) {
        glDrawElements(GL_TRIANGLES, indices_, GL_UNSIGNED_INT, nullptr);
    } else if (verts_ > 0) {
        glDrawArrays(GL_TRIANGLES, 0, verts_);
    }
}

bool Mesh::load(const resource_ptr& _resource) {
    LOG_INFO("Loading model: {}", _resource->location());
    set_name(_resource->base());

    return load(_resource->string());
}

bool Mesh::load(const std::string& _data) {
    auto start = std::chrono::system_clock::now();

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

    auto stream = std::istringstream(_data);

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, &stream)) {
        LOG_ERROR("Failed to load model: {}", err);
        return false;
    }

    for (const auto& shape : shapes) {
        for (const auto& index : shape.mesh.indices) {
            Vertex vertex = {};

            vertex.position = {
                attrib.vertices[3 * index.vertex_index + 0],
                attrib.vertices[3 * index.vertex_index + 1],
                attrib.vertices[3 * index.vertex_index + 2],
            };
            if (!attrib.normals.empty()) {
                vertex.normal = {
                    attrib.normals[3 * index.normal_index + 0],
                    attrib.normals[3 * index.normal_index + 1],
                    attrib.normals[3 * index.normal_index + 2],
                };
            }

            if (!attrib.texcoords.empty()) {
                vertex.uv = {
                    attrib.texcoords[2 * index.texcoord_index + 0],
                    attrib.texcoords[2 * index.texcoord_index + 1],
                };
            }

            if (!uniqueVertices.count(vertex)) {
                uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(vertex);
            }

            indices.push_back(uniqueVertices[vertex]);
        }
    }

    bind();
    indices_ = static_cast<int32_t>(indices.size());
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);
    unbind();

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double, std::milli> diff = end-start;
    LOG_DEBUG("Loaded model in {} ms", diff.count());

    return true;
}

MeshHandler::MeshHandler()
: AssetHandler("mesh") {}

void MeshHandler::load(const resource_ptr& _resource) {
    auto mesh = std::make_shared<Mesh>();

    if (!mesh->load(_resource)) {
        throw std::runtime_error("Model failed to load");
    }

    items_[mesh->get_name()] = mesh;
}

void MeshHandler::gui_inspector() {
    if (ImGui::TreeNode("Mesh")) {
        for (auto const &c: items_) {
            ImGui::BulletText("%s", c.first.c_str());
        }
        ImGui::TreePop();
    }
}

mesh_ptr ember::create_mesh_quad() {
    auto m = std::make_shared<Mesh>();
    m->set_name(BUILTIN_MESH_QUAD);

    m->upload(std::vector<Vertex>{
        Vertex({-1.0, 1.0, 0.0}, {0.0, 0.0, 1.0}, {0.0, 1.0}),
        Vertex({-1.0, -1.0, 0.0}, {0.0, 0.0, 1.0}, {0.0, 0.0}),
        Vertex({1.0, -1.0, 0.0}, {0.0, 0.0, 1.0}, {1.0, 0.0}),
        Vertex({-1.0, 1.0, 0.0}, {0.0, 0.0, 1.0}, {0.0, 1.0}),
        Vertex({1.0, -1.0, 0.0}, {0.0, 0.0, 1.0}, {1.0, 0.0}),
        Vertex({1.0, 1.0, 0.0}, {0.0, 0.0, 1.0}, {1.0, 1.0}),
    });

    return m;
}

mesh_ptr ember::create_mesh_quad_back() {
    auto m = std::make_shared<Mesh>();
    m->set_name(BUILTIN_MESH_QUAD_BACK);

    m->upload(std::vector<Vertex>{
        Vertex({-1.0, 1.0, 1.0}, {0.0, 0.0, 1.0}, {0.0, 1.0}),
        Vertex({-1.0, -1.0, 1.0}, {0.0, 0.0, 1.0}, {0.0, 0.0}),
        Vertex({1.0, -1.0, 1.0}, {0.0, 0.0, 1.0}, {1.0, 0.0}),
        Vertex({-1.0, 1.0, 1.0}, {0.0, 0.0, 1.0}, {0.0, 1.0}),
        Vertex({1.0, -1.0, 1.0}, {0.0, 0.0, 1.0}, {1.0, 0.0}),
        Vertex({1.0, 1.0, 1.0}, {0.0, 0.0, 1.0}, {1.0, 1.0}),
    });

    return m;
}
