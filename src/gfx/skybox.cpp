/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/skybox.h>

#include <glm/gtc/matrix_transform.hpp>

#include <ember/core/log.h>
#include <ember/core/asset.h>
#include <ember/gfx/framebuffer.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/shader.h>
#include <ember/gfx/texture.h>

using namespace ember;

glm::mat4 rotation_matrices[6] = {
        glm::lookAt(glm::vec3(0), glm::vec3(-1, 0, 0), glm::vec3(0, -1, 0)),
        glm::lookAt(glm::vec3(0), glm::vec3(1, 0, 0), glm::vec3(0, -1, 0)),
        glm::lookAt(glm::vec3(0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1)),
        glm::lookAt(glm::vec3(0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1)),
        glm::lookAt(glm::vec3(0), glm::vec3(0, 0, 1), glm::vec3(0, -1, 0)),
        glm::lookAt(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, -1, 0)),
};

static texture_cubemap_ptr generate_irradiance_map(const TextureCubemap* _source, const Framebuffer* _fbo, const Mesh* _mesh) {
    auto irradiance = std::make_shared<TextureCubemap>(_source->get_format(), 32);
    auto shader = AssetManager::get_asset<Shader>("pbr/irradiance");

    shader->bind();
    shader->set_uniform("v_projection_matrix", glm::perspective(glm::half_pi<float>(), 1.0f, 0.1f, 10.0f));

    _source->activate(0);

    glViewport(0, 0, 32, 32);

    for (uint32_t i = 0; i < 6; i++) {
        shader->set_uniform("v_view_matrix", rotation_matrices[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irradiance->get_reference(), 0);
        _fbo->clear_buffers();
        _mesh->draw();
    }

    shader->unbind();

    return irradiance;
}

static texture_cubemap_ptr generate_specular_map(const TextureCubemap* _source, const Framebuffer* _fbo, const Mesh* _mesh) {
    auto specular = std::make_shared<TextureCubemap>(_source->get_format(), 512);
    auto shader = AssetManager::get_asset<Shader>("pbr/specular");

    specular->bind();
    specular->set_wrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
    specular->set_filter_min(GL_LINEAR_MIPMAP_LINEAR);
    specular->generate_mipmaps();

    shader->bind();
    shader->set_uniform("v_projection_matrix", glm::perspective(glm::half_pi<float>(), 1.0f, 0.1f, 2.0f));

    _source->activate(0);

    glViewport(0, 0, 512, 512);

    uint32_t max_miplevels = 5;

    for (uint32_t mip = 0; mip < max_miplevels; mip++) {
        auto size = (uint32_t)(512 * glm::pow(0.5, mip));
        glViewport(0, 0, size, size);

        float roughness = float(mip) / float(max_miplevels - 1);
        shader->set_uniform("u_roughness", roughness);

        for (uint32_t i = 0; i < 6; i++) {
            shader->set_uniform("v_view_matrix", rotation_matrices[i]);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, specular->get_reference(), mip);
            _fbo->clear_buffers();
            _mesh->draw();
        }
    }

    shader->unbind();

    return specular;
}

Skybox::Skybox(texture_cubemap_ptr _source)
        : radiance_map_(std::move(_source)) {
    if (!radiance_map_) {
        throw std::runtime_error("radiance map cannot be null");
    }

    generate_maps();
}

Skybox::Skybox(texture_cubemap_ptr _rad, texture_cubemap_ptr _irr, texture_cubemap_ptr _spec)
        : radiance_map_(std::move(_rad))
        , irradiance_map_(std::move(_irr))
        , specular_map_(std::move(_spec)) {}

const texture_cubemap_p Skybox::get_radiance() const {
    return radiance_map_.get();
}

const texture_cubemap_p Skybox::get_irradiance() const {
    return irradiance_map_.get();
}

const texture_cubemap_p Skybox::get_specular() const {
    return specular_map_.get();
}

void Skybox::generate_maps() {
    LOG_INFO("Generating maps for skybox...");

    auto fbo = std::make_shared<Framebuffer>();
    auto mesh = AssetManager::get_asset<Mesh>("cube.obj");

    fbo->bind();
    mesh->bind();

    // Generate irradiance map.
    irradiance_map_ = generate_irradiance_map(radiance_map_.get(), fbo.get(), mesh);

    // Generate specular map.
    specular_map_ = generate_specular_map(radiance_map_.get(), fbo.get(), mesh);

    mesh->unbind();
    fbo->unbind();
}
