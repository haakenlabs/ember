/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/texture.h>

using namespace ember;

Texture::Texture(GLenum _type, TextureFormat _format)
: type_(_type)
, layers_(1)
, resizable_(false)
, format_(_format)
, storage_(format_to_storage(_format))
, internal_(format_to_internal(_format))
, glformat_(format_to_glformat(_format)) {
    glGenTextures(1, &reference_);

    bind();
    set_wrap_r(GL_CLAMP_TO_BORDER);
    set_wrap_s(GL_CLAMP_TO_BORDER);
    set_wrap_t(GL_CLAMP_TO_BORDER);
    set_filter(GL_LINEAR, GL_LINEAR);
    unbind();
}

Texture::~Texture() {
    glDeleteTextures(1, &reference_);
}

void Texture::bind() const {
    glBindTexture(type_, reference_);
}

void Texture::unbind() const {
    glBindTexture(type_, 0);
}

void Texture::activate(uint32_t _unit) const {
    glActiveTexture(GL_TEXTURE0 + _unit);
    bind();
}

uint32_t Texture::get_type() const {
    return type_;
}

uint32_t Texture::get_reference() const {
    return reference_;
}

GLenum Texture::get_storage() const {
    return storage_;
}

GLenum Texture::get_internal() const {
    return internal_;
}

GLenum Texture::get_glformat() const {
    return glformat_;
}

GLenum Texture::get_filter_min() const {
    return filter_min_;
}

GLenum Texture::get_filter_mag() const {
    return filter_mag_;
}

GLenum Texture::get_wrap_r() const {
    return wrap_r_;
}

GLenum Texture::get_wrap_s() const {
    return wrap_s_;
}

GLenum Texture::get_wrap_t() const {
    return wrap_t_;
}

glm::ivec2 Texture::get_size() const {
    return size_;
}

void Texture::set_wrap(GLenum _r, GLenum _s, GLenum _t) {
    set_wrap_r(_r);
    set_wrap_s(_s);
    set_wrap_t(_t);
}

void Texture::set_wrap_r(GLenum _r) {
    wrap_r_ = _r;
    glTexParameteri(type_, GL_TEXTURE_WRAP_R, wrap_r_);
    if (wrap_r_ == GL_CLAMP_TO_BORDER) {
        float color[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
    }
}

void Texture::set_wrap_s(GLenum _s) {
    wrap_s_ = _s;
    glTexParameteri(type_, GL_TEXTURE_WRAP_S, wrap_s_);
    if (wrap_s_ == GL_CLAMP_TO_BORDER) {
        float color[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
    }
}

void Texture::set_wrap_t(GLenum _t) {
    wrap_t_ = _t;
    glTexParameteri(type_, GL_TEXTURE_WRAP_T, wrap_t_);
    if (wrap_t_ == GL_CLAMP_TO_BORDER) {
        float color[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
    }
}

void Texture::set_filter(GLenum _filter_min, GLenum _filter_mag) {
    set_filter_min(_filter_min);
    set_filter_mag(_filter_mag);
}

void Texture::set_filter_min(GLenum _min) {
    filter_min_ = _min;
    glTexParameteri(type_, GL_TEXTURE_MIN_FILTER, filter_min_);
}

void Texture::set_filter_mag(GLenum _mag) {
    filter_mag_ = _mag;
    glTexParameteri(type_, GL_TEXTURE_MAG_FILTER, filter_mag_);
}

void Texture::set_size(glm::ivec2 _size) {
    if (resizable_ && _size.x > 0 && _size.y > 0) {
        size_ = _size;
        upload();
    }
}

TextureFormat Texture::get_format() const {
    return format_;
}

void Texture::set_format(TextureFormat _format) {
    format_ = _format;

    storage_ = format_to_storage(_format);
    internal_ = format_to_internal(_format);
    glformat_ = format_to_glformat(_format);
}

void Texture::upload() {
    upload_data(nullptr);
}

void Texture::generate_mipmaps() {
    glGenerateMipmap(type_);
}

GLenum ember::format_to_storage(TextureFormat _format) {
    switch (_format) {
        case TextureFormatDefaultColor:
        case TextureFormatR8:
        case TextureFormatRG8:
        case TextureFormatRGB8:
        case TextureFormatRGBA8:
        case TextureFormatStencil8:
            return GL_UNSIGNED_BYTE;
        case TextureFormatR16:
        case TextureFormatRG16:
        case TextureFormatRGB16:
        case TextureFormatDefaultHDRColor:
        case TextureFormatRGBA16:
            return GL_FLOAT;
        case TextureFormatRGBA16UI:
            return GL_UNSIGNED_SHORT;
        case TextureFormatR32:
        case TextureFormatRG32:
        case TextureFormatRGB32:
        case TextureFormatRGBA32:
            return GL_FLOAT;
        case TextureFormatRGB32UI:
        case TextureFormatRGBA32UI:
            return GL_UNSIGNED_INT;
        case TextureFormatDefaultDepth:
        case TextureFormatDepth16:
        case TextureFormatDepth24:
            return GL_FLOAT;
        case TextureFormatDepth24Stencil8:
            return GL_UNSIGNED_INT_24_8;
    }

    return 0;
}

GLenum ember::format_to_internal(TextureFormat _format) {
    switch (_format) {
        case TextureFormatR8:
            return GL_R8;
        case TextureFormatRG8:
            return GL_RG8;
        case TextureFormatRGB8:
            return GL_RGB;
        case TextureFormatDefaultColor:
        case TextureFormatRGBA8:
            return GL_RGBA;
        case TextureFormatR16:
            return GL_R16F;
        case TextureFormatRG16:
            return GL_RG16F;
        case TextureFormatRGB16:
            return GL_RGB16F;
        case TextureFormatDefaultHDRColor:
        case TextureFormatRGBA16:
            return GL_RGBA16F;
        case TextureFormatR32:
            return GL_R32F;
        case TextureFormatRG32:
            return GL_RG32F;
        case TextureFormatRGB32:
            return GL_RGB32F;
        case TextureFormatRGBA32:
            return GL_RGBA32F;
        case TextureFormatRGB32UI:
            return GL_RGB32UI;
        case TextureFormatRGBA32UI:
            return GL_RGBA32UI;
        case TextureFormatDepth16:
            return GL_DEPTH_COMPONENT16;
        case TextureFormatDefaultDepth:
        case TextureFormatDepth24:
            return GL_DEPTH_COMPONENT24;
        case TextureFormatDepth24Stencil8:
            return GL_DEPTH24_STENCIL8;
        case TextureFormatStencil8:
            return GL_STENCIL_INDEX8;
        case TextureFormatRGBA16UI:
            return GL_RGBA16UI;
    }

    return 0;
}

GLenum ember::format_to_glformat(TextureFormat _format) {
    switch (_format) {
        case TextureFormatR8:
        case TextureFormatR16:
        case TextureFormatR32:
            return GL_RED;
        case TextureFormatRG8:
        case TextureFormatRG16:
        case TextureFormatRG32:
            return GL_RG;
        case TextureFormatRGB8:
        case TextureFormatRGB16:
        case TextureFormatRGB32:
            return GL_RGB;
        case TextureFormatRGB32UI:
            return GL_RGB_INTEGER;
        case TextureFormatDefaultColor:
        case TextureFormatRGBA8:
        case TextureFormatDefaultHDRColor:
        case TextureFormatRGBA16:
        case TextureFormatRGBA16UI:
        case TextureFormatRGBA32:
            return GL_RGBA;
        case TextureFormatRGBA32UI:
            return GL_RGBA_INTEGER;
        case TextureFormatDefaultDepth:
        case TextureFormatDepth16:
        case TextureFormatDepth24:
            return GL_DEPTH_COMPONENT;
        case TextureFormatDepth24Stencil8:
            return GL_DEPTH24_STENCIL8;
        case TextureFormatStencil8:
            return GL_STENCIL_INDEX8;
    }

    return 0;
}
