/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/renderer.h>

#include <ember/core/transform.h>
#include <ember/gfx/builtin.h>
#include <ember/gfx/effect.h>
#include <ember/gfx/framebuffer.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/shader.h>
#include <ember/gfx/skybox.h>
#include <ember/gfx/texture.h>
#include <ember/scene/light.h>

using namespace ember;

Renderer* Renderer::ref__ = nullptr;

static std::shared_ptr<Texture2D> generate_brdf_lut() {
    auto fbo = std::make_shared<Framebuffer>();
    auto lut = std::make_shared<Texture2D>(TextureFormatRG16, glm::ivec2(512));
    auto shader = AssetManager::get_asset<Shader>("pbr/brdf");
    auto mesh = create_mesh_quad();

    lut->bind();
    lut->set_wrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
    lut->unbind();

    fbo->bind();
    glViewport(0, 0, 512, 512);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lut->get_reference(), 0);

    shader->bind();
    mesh->bind();

    mesh->draw();

    mesh->unbind();
    shader->unbind();
    fbo->unbind();

    return lut;
}


Renderer::Renderer()
: active_mode_(RenderModeDeferred)
, visualize_buffer_(RendererBufferNone) {
    ref__ = this;

    builtin_shaders_[BUILTIN_SHADER_RTT] = std::make_shared<Shader>(
        BUILTIN_SHADER_RTT, false, SRC_SHADER_RTT);
    builtin_shaders_[BUILTIN_SHADER_CUBEMAP_CONV] = std::make_shared<Shader>(
        BUILTIN_SHADER_CUBEMAP_CONV, false, SRC_SHADER_CUBEMAP_CONV);

    builtin_meshes_[BUILTIN_MESH_CUBE] = std::make_shared<Mesh>(SRC_MODEL_CUBE);
    builtin_meshes_[BUILTIN_MESH_QUAD] = create_mesh_quad();
    builtin_meshes_[BUILTIN_MESH_QUAD_BACK] = create_mesh_quad_back();

    fbo_tmp_ = std::make_shared<Framebuffer>();
    fbo_ = std::make_shared<Framebuffer>();

    primary_shader_ = AssetManager::get_asset_ptr<Shader>("standard");
    skybox_shader_ = AssetManager::get_asset_ptr<Shader>("skybox");
    debug_shader_ = AssetManager::get_asset_ptr<Shader>("debug");

    tex_gbuffer_0_ = std::make_shared<Texture2D>(TextureFormatRGBA32);
    tex_gbuffer_1_ = std::make_shared<Texture2D>(TextureFormatRGBA32UI);
    tex_hdr_target_ = std::make_shared<Texture2D>(TextureFormatDefaultHDRColor);
    tex_ldr_target_ = std::make_shared<Texture2D>(TextureFormatDefaultColor);
    tex_depth_ = std::make_shared<Texture2D>(TextureFormatDepth24);
    tex_brdf_lut_ = generate_brdf_lut();

    mesh_ = builtin_meshes_[BUILTIN_MESH_QUAD];

    effect_processor_ = std::make_shared<EffectProcessor>(fbo_, tex_hdr_target_, tex_ldr_target_);
}

Renderer::~Renderer() {
    ref__ = nullptr;
}

void Renderer::render(
    renderable_p _renderable,
    const std::vector<drawable_ptr>& _drawables,
    const std::vector<light_ptr>& _lights
) {
    fbo_->bind();

    active_mode_ = RenderModeDeferred;

    /* GBuffer Geometry */

    fbo_->attach(GL_COLOR_ATTACHMENT0, tex_gbuffer_0_.get());
    fbo_->attach(GL_COLOR_ATTACHMENT1, tex_gbuffer_1_.get());
    fbo_->apply_draw_buffers({GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1});
    fbo_->clear_buffers();

    for (auto const &v : _drawables) {
        if (v->is_deferrable()) {
            v->draw(_renderable);
        }
    }

    /* Deferred Ambient */

    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LEQUAL);

    fbo_->attach(GL_COLOR_ATTACHMENT0, tex_hdr_target_.get());
    fbo_->apply_draw_buffers({GL_COLOR_ATTACHMENT0});
    fbo_->clear_buffers();

    primary_shader_->bind();
    primary_shader_->set_subroutine(ShaderComponentFragment, "deferred_pass_ambient");
    primary_shader_->set_uniform("u_mvp_matrix", _renderable->get_projection_matrix() * _renderable->get_view_matrix());
    primary_shader_->set_uniform("v_model_matrix", glm::mat4(1));
    primary_shader_->set_uniform("v_view_matrix", glm::mat4(1));
    primary_shader_->set_uniform("v_projection_matrix", glm::mat4(1));
    primary_shader_->set_uniform("f_camera", _renderable->get_position());

    tex_gbuffer_0_->activate(0);
    tex_gbuffer_1_->activate(1);
    tex_depth_->activate(2);

    if (skybox_) {
        skybox_->get_radiance()->activate(3);
        skybox_->get_irradiance()->activate(4);
        skybox_->get_specular()->activate(5);
    }
    tex_brdf_lut_->activate(6);

    mesh_->bind();
    mesh_->draw();

    /* Deferred Lighting */

    glEnable(GL_BLEND);

    primary_shader_->set_subroutine(ShaderComponentFragment, "deferred_pass_lighting");

    for (auto const &l : _lights) {
        primary_shader_->set_uniform("u_light_color", l->get_color().as_vec3());
        primary_shader_->set_uniform("u_light_position", l->get_transform()->get_position());
        primary_shader_->set_uniform("u_light_type", (int)l->get_type());

        mesh_->draw();
    }

    mesh_->unbind();

    glDisable(GL_BLEND);

    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);

    /* Forward Pass */

    active_mode_ = RenderModeForward;

    for (auto const &v : _drawables) {
        if (!v->is_deferrable()) {
            v->draw(_renderable);
        }
    }

    /* Skybox */

    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LEQUAL);

    skybox_shader_->bind();
    skybox_shader_->set_uniform("v_view_matrix", _renderable->get_view_matrix());
    skybox_shader_->set_uniform("v_projection_matrix", _renderable->get_projection_matrix());

    if (skybox_) {
        skybox_->get_radiance()->activate(0);
    }

    mesh_->bind();
    mesh_->draw();

    /* Effects */

    effect_processor_->process_effects();

    /* Debug Pass */

    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);

    if (visualize_buffer_) {
        glDisable(GL_DEPTH_TEST);
        debug_shader_->bind();
        switch (visualize_buffer_) {
            case RendererBufferPositions:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_position");
                break;
            case RendererBufferNormals:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_normal");
                break;
            case RendererBufferDepth:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_depth");
                break;
            case RendererBufferAlbedo:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_albedo");
                break;
            case RendererBufferMetallic:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_metallic");
                break;
            case RendererBufferRoughness:
                debug_shader_->set_subroutine(ShaderComponentFragment, "debug_visualize_roughness");
                break;
            default:
                break;
        }

        mesh_->draw();
        debug_shader_->unbind();
        glEnable(GL_DEPTH_TEST);
    }

    mesh_->unbind();

    /* Final Blit */

    fbo_->unbind();

    Framebuffer::blit_framebuffers(fbo_.get(), get_current_framebuffer());
}

void Renderer::set_size(glm::ivec2 _size) {
    size_ = _size;

    fbo_->set_size(size_);
    tex_gbuffer_0_->set_size(size_);
    tex_gbuffer_1_->set_size(size_);
    tex_hdr_target_->set_size(size_);
    tex_ldr_target_->set_size(size_);
    tex_depth_->set_size(size_);
}

glm::ivec2 Renderer::get_size() const {
    return size_;
}

void Renderer::push_framebuffer(framebuffer_ptr _fbo) {
    if (!ref__->fbo_stack_.empty()) {
        ref__->fbo_stack_.top()->bound_ = false;
    }

    _fbo->bound_ = true;
    glBindFramebuffer(GL_FRAMEBUFFER, _fbo->reference_);
    glViewport(0, 0, _fbo->size_.x, _fbo->size_.y);
    ref__->fbo_stack_.push(std::move(_fbo));
}

void Renderer::pop_framebuffer() {
    if (!ref__->fbo_stack_.empty()) {
        ref__->fbo_stack_.top()->bound_ = false;
        ref__->fbo_stack_.pop();

        if (!ref__->fbo_stack_.empty()) {
            auto &fbo = ref__->fbo_stack_.top();
            fbo->bound_ = true;
            glBindFramebuffer(GL_FRAMEBUFFER, fbo->reference_);
            glViewport(0, 0, fbo->size_.x, fbo->size_.y);
        } else {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glViewport(0, 0, ref__->size_.x, ref__->size_.y);
        }
    }
}

framebuffer_p Renderer::get_current_framebuffer() {
    if (ref__->fbo_stack_.empty()) {
        return nullptr;
    }

    return ref__->fbo_stack_.top().get();
}

framebuffer_p Renderer::get_temp_framebuffer() {
    return ref__->fbo_tmp_.get();
}

shader_p Renderer::get_builtin_shader(const std::string& _name) {
    try {
        return ref__->builtin_shaders_.at(_name).get();
    } catch (const std::out_of_range& e) {
        throw std::invalid_argument("no such shader: " + _name);
    }
}

mesh_p Renderer::get_builtin_mesh(const std::string& _name) {
    try {
        return ref__->builtin_meshes_.at(_name).get();
    } catch (const std::out_of_range& e) {
        throw std::invalid_argument("no such mesh: " + _name);
    }
}
