/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/gfx/texture.h>

#include <chrono>

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#include <stb/stb_image.h>

#include <imgui/imgui.h>

#include <ember/core/log.h>
#include <ember/core/resource.h>

using namespace ember;

Texture2D::Texture2D(TextureFormat _format, bool _resizable)
: Texture2D(_format, glm::ivec2(0), _resizable) {}

Texture2D::Texture2D(TextureFormat _format, glm::ivec2 _size, bool _resizable)
: Texture(GL_TEXTURE_2D, _format)
{
    class_name_ = "Texture2D";
    name_ = "Texture2D";
    size_ = _size;
    resizable_ = _resizable;

    bind();
    upload();
}

void Texture2D::upload_data(void* _data) {
    bind();
    glTexImage2D(type_, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, _data);
    unbind();
}

bool Texture2D::load(const resource_ptr& _resource) {
    auto start = std::chrono::system_clock::now();

    stbi_set_flip_vertically_on_load(true);
    int32_t width = 0;
    int32_t height = 0;
    int32_t nr_components = 0;

    if (stbi_is_hdr_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size())) {
        auto ptr = stbi_loadf_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size(), &width, &height, &nr_components, 0);
        if (!ptr) {
            LOG_ERROR("Failed to load HDR image");
            return false;
        }

        set_format(TextureFormatRGB16);
        size_= glm::ivec2(width, height);
        set_name(_resource->base());

        upload_data(ptr);

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> diff = end-start;

        printf("Loaded HDR image in %f ms. %dx%d (%d)\n", diff.count(), width, height, nr_components);
        stbi_image_free(ptr);

        return true;
    } else {
        auto data = stbi_load_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size(), &width, &height, &nr_components, 0);
        if (!data) {
            LOG_ERROR("Failed to load standard image");
            return false;
        }

        TextureFormat fmt;

        switch (nr_components) {
            case 1:
                fmt = TextureFormatR8;
                break;
            case 2:
                fmt = TextureFormatRG8;
                break;
            case 3:
                fmt = TextureFormatRGB8;
                break;
            case 4:
                fmt = TextureFormatRGBA8;
                break;
            default:
                throw std::invalid_argument("Invalid number of channels");
        }

        set_format(fmt);
        size_= glm::ivec2(width, height);
        set_name(_resource->base());

        upload_data(data);

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> diff = end-start;

        printf("Loaded standard image in %f ms. %dx%d (%d)\n", diff.count(), width, height, nr_components);
        stbi_image_free(data);

        return true;
    }
}

void Texture2D::gui_inspector() {
    ImGui::Text("Texture type: GL_TEXTURE_2D");
    ImGui::Text("Reference: %u", reference_);

    auto scaled_h = int32_t((float(size_.y) / float(size_.x)) * 256.f);

    ImGui::Image(
        (ImTextureID)(intptr_t)reference_,
        ImVec2(256, scaled_h),
        ImVec2(0,1), ImVec2(1,0),
        ImColor(255,255,255,255), ImColor(255,255,255,128));

}

TextureHandler::TextureHandler()
: AssetHandler("texture") {}

void TextureHandler::load(const resource_ptr& _resource) {
    auto t = std::make_shared<Texture2D>(TextureFormatDefaultColor);

    if (!t->load(_resource)) {
        throw std::runtime_error("Texture failed to load");
    }

    items_[t->get_name()] = t;
}

void TextureHandler::gui_inspector() {
    if (ImGui::TreeNode("Texture")) {
        for (auto const &c: items_) {
            if (ImGui::TreeNode(c.first.c_str())) {
                c.second->gui_inspector();
                ImGui::TreePop();
            }
        }
        ImGui::TreePop();
    }
}