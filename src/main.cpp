#include <iostream>

#include <ember/core/app.h>
#include <ember/core/log.h>

int main() {
    auto app = std::make_unique<ember::App>();

    app->run();

    return 0;
}
