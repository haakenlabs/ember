/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/asset.h>

#include <chrono>

#include <imgui/imgui.h>

#include <ember/util/file.h>
#include <ember/util/json.h>
#include <ember/core/log.h>
#include <ember/core/resource.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/shader.h>
#include <ember/gfx/texture.h>

using namespace ember;

AssetManager* AssetManager::ref__ = nullptr;

AssetManager::AssetManager()
: metrics_({}) {
    ref__ = this;

    register_handler(std::make_shared<MeshHandler>());
    register_handler(std::make_shared<ShaderHandler>());
    register_handler(std::make_shared<TextureHandler>());
    register_handler(std::make_shared<CubemapHandler>());

    /* Load builtin assets */
    add_manifest("resources/builtin.json");
    process(true);
    reset_metrics();
    load_queue_.clear();
}

AssetManager::~AssetManager() {
    ref__ = nullptr;
}

void AssetManager::register_handler(std::shared_ptr<GenericAssetHandler> _handler) {
    if (!has_handler(_handler->get_type())) {
        ref__->handlers_[_handler->get_type()] = _handler;
        LOG_INFO("Registered handler: {}", _handler->get_type().c_str());
    }
}

bool AssetManager::has_handler(const std::string &_handler) {
    return ref__->handlers_.find(_handler) != ref__->handlers_.cend();
}

size_t AssetManager::size() {
    size_t s = 0;

    for (auto const &c : ref__->handlers_) {
        s += c.second->count();
    }

    return s;
}

size_t AssetManager::size_by_type(const std::string &_name) {
    auto itr = ref__->handlers_.find(_name);
    if (itr != ref__->handlers_.cend()) {
        return itr->second->count();
    }

    return 0;
}

void AssetManager::add_manifest(const std::string &_filename) {
    auto res = Resource(_filename);
    auto doc = load_json(read_file(_filename));

    auto &types = doc["assets"];
    for (auto itr = types.MemberBegin(); itr != types.MemberEnd(); ++itr) {
        for (auto list_itr = itr->value.Begin(); list_itr != itr->value.End(); ++list_itr) {
            ref__->load_queue_.emplace_back(itr->name.GetString(), res.dir_prefix() + "/" + list_itr->GetString());
            ref__->metrics_.total_++;
        }
    }
}

void AssetManager::process(bool _unbounded) {
    std::string cur_type;
    GenericAssetHandler *handler = nullptr;

    if (done()) {
        return;
    }

    LOG_DEBUG("There are {} assets to process...",
              ref__->metrics_.total_-ref__->metrics_.processed_);
    if (_unbounded) {
        LOG_DEBUG("Unbounded process requested...");
    }

    auto stop = std::chrono::system_clock::now() + std::chrono::milliseconds(15);

    while (!done()) {
        if (!_unbounded && std::chrono::system_clock::now() > stop) {
            LOG_DEBUG("Process ran out of time, breaking...");
            break;
        }

        auto v = ref__->load_queue_[ref__->metrics_.processed_];
        ref__->metrics_.processed_++;
        ref__->metrics_.current_asset_ = v.filename_;

        LOG_DEBUG("Loading asset: {}", v.filename_.c_str());

        if (cur_type != v.type_) {
            auto itr = ref__->handlers_.find(v.type_);
            if (itr == ref__->handlers_.cend()) {
                LOG_ERROR("No asset handler for type: {}", v.type_.c_str());
                continue;
            }
            cur_type = v.type_;
            handler = itr->second.get();
        }


        auto start = std::chrono::system_clock::now();
        handler->load(std::make_shared<Resource>(v.filename_));
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> diff = end-start;

        LOG_DEBUG("Loaded asset in {} ms", diff.count());
    }
}

void AssetManager::reset_metrics() {
    ref__->metrics_.total_ = 0;
    ref__->metrics_.processed_ = 0;
    ref__->metrics_.current_asset_ = "";
}

AssetManager::Metrics AssetManager::get_metrics() {
    return ref__->metrics_;
}

float AssetManager::Metrics::progress() const {
    if (!total_) {
        return 1.0f;
    }

    return float(processed_) / float(total_);
}

bool AssetManager::done() {
    return ref__->metrics_.total_-ref__->metrics_.processed_ == 0;
}

void AssetManager::gui_inspector() {
    for (auto const &h : ref__->handlers_) {
        h.second->gui_inspector();
    }
}
