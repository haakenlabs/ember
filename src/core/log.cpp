/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/log.h>

#include <iostream>

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/fmt/fmt.h>

using namespace ember;

const ImVec4 COLOR_TRACE{1.f, 1.f, 1.f, 1.f};
const ImVec4 COLOR_DEBUG{1.f, 1.f, 1.f, 1.f};
const ImVec4 COLOR_INFO{1.f, 1.f, 1.f, 1.f};
const ImVec4 COLOR_WARN{1.f, 1.f, 1.f, 1.f};
const ImVec4 COLOR_ERROR{1.f, 1.f, 1.f, 1.f};
const ImVec4 COLOR_FATAL{1.f, 1.f, 1.f, 1.f};

const char* DEFAULT_LOG_FORMAT = "[%Y-%m-%d %T.%f] [%^%L%$] %v";

static Log* std_logger__ = nullptr;

Log::Log()
: level_(trace) {
    if (std_logger__) {
        throw std::runtime_error("Only one instance of Log allowed");
    }

    std_logger__ = this;
    gui_sink_ = std::make_shared<GUILogSink>();

    std::vector<spdlog::sink_ptr> sinks;
    sinks.push_back(std::make_shared<spdlog::sinks::stderr_color_sink_st>());
    sinks.push_back(gui_sink_);

    logger_ = std::make_shared<spdlog::logger>("stdlogger", sinks.begin(), sinks.end());
    logger_->set_pattern(DEFAULT_LOG_FORMAT);
    logger_->set_level(spdlog::level::trace);
    logger_->flush_on(spdlog::level::critical);
}

Log::~Log() {
    std_logger__ = nullptr;
}

void Log::set_log_level(LogLevel _level) {
    level_ = _level;
    logger_->set_level(static_cast<spdlog::level::level_enum>(_level));
}

LogLevel Log::get_log_level() const {
    return level_;
}

void Log::gui_draw() {
    gui_sink_->draw();
}

void GUILogSink::clear() {
    std::unique_lock<std::shared_timed_mutex> lock(records_mutex_);
    records_.clear();
}

void GUILogSink::toggle_wrap() {
    wrap_ = !wrap_;
}

void GUILogSink::toggle_scroll_lock() {
    scroll_lock_ = !scroll_lock_;
}

void GUILogSink::draw() {
    bool need_pop_style_var = false;

    /* Clear Logs */

    if (ImGui::Button("Clear Logs")) {
        clear();
    }
    if (ImGui::IsItemHovered()) {
        ImGui::SetTooltip("Clear log history");
    }
    /* Log Level */

    ImGui::SameLine();
    if (ImGui::Button("Level")) {
        ImGui::OpenPopup("log_levels_popup");
    }
    if (ImGui::IsItemHovered()) {
        ImGui::SetTooltip("Change the logging level");
    }
    if (ImGui::BeginPopup("log_levels_popup")) {
        int level = static_cast<int>(get_log_level());
        auto fmt = std::string("%u (")
                .append(spdlog::level::to_c_str(get_logger()->level()))
                .append(")");

        if (ImGui::SliderInt("Level", &level, 0, 6, fmt.c_str())) {
            set_log_level(LogLevel(level));
        }
        ImGui::EndPopup();
    }

    /* Wrap Lines */

    ImGui::SameLine();
    need_pop_style_var = false;
    if (wrap_) {
        // Highlight the button
        ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 2.0f);
        ImGui::PushStyleColor(ImGuiCol_Border,
                              ImGui::GetStyleColorVec4(ImGuiCol_TextSelectedBg));
        need_pop_style_var = true;
    }
    if (ImGui::Button("Wrap Lines")) {
        toggle_wrap();
    }
    if (ImGui::IsItemHovered()) {
        ImGui::SetTooltip("Toggle soft wraps");
    }
    if (need_pop_style_var) {
        ImGui::PopStyleColor();
        ImGui::PopStyleVar();
    }

    /* Scroll Lock */

    ImGui::SameLine();
    need_pop_style_var = false;
    if (scroll_lock_) {
        // Highlight the button
        ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 2.0f);
        ImGui::PushStyleColor(ImGuiCol_Border,
                              ImGui::GetStyleColorVec4(ImGuiCol_TextSelectedBg));
        need_pop_style_var = true;
    }
    if (ImGui::Button("Scroll Lock")) {
        toggle_scroll_lock();
    }
    if (ImGui::IsItemHovered()) {
        ImGui::SetTooltip("Toggle automatic scrolling to the bottom");
    }
    if (need_pop_style_var) {
        ImGui::PopStyleColor();
        ImGui::PopStyleVar();
    }

    /* Filter */

    ImGui::SameLine();
    display_filter_.Draw("Filter", -100.0f);

    /* Format Field */


    ImGui::Separator();
    ImGui::BeginChild("log_viewer", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);
    {
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 1));

        for (auto const &record : records_) {
            if (!display_filter_.IsActive() || display_filter_.PassFilter(record.message_.c_str())) {
                ImGui::TextUnformatted(record.message_.c_str());
            }
        }

        ImGui::PopStyleVar();
    }

    if (!scroll_lock_ && scroll_to_bottom_) {
        ImGui::SetScrollHere(1.0f);
    }
    scroll_to_bottom_ = false;

    ImGui::EndChild();
}

void GUILogSink::sink_it_(const spdlog::details::log_msg& _msg) {
    fmt::memory_buffer formatted;
    sink::formatter_->format(_msg, formatted);

    auto record = LogRecord{fmt::to_string(formatted)};

    records_.push_back(record);
    scroll_to_bottom_ = true;
}

void GUILogSink::flush_() {}

LogLevel ember::get_log_level() {
    return std_logger__->get_log_level();
}

void ember::set_log_level(LogLevel _level) {
    std_logger__->set_log_level(_level);
}

spdlog::logger* ember::get_logger() {
    return std_logger__->logger_.get();
}