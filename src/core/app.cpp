/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/app.h>

#include <csignal>

#include <imgui/imgui.h>

#define MAX_FRAME_SKIP 5

using namespace ember;

static const char* VERSION = "0.0.1";

bool App::running__ = false;

static void signal_exit(int signal) {
    App::quit();
}

App::App()
: log_(std::make_unique<Log>()) {
    instance_ = std::make_unique<ObjectManager>();
    window_ = std::make_unique<Window>();
    input_ = std::make_unique<Input>();
    time_ = std::make_unique<Time>();
    asset_manager_ = std::make_unique<AssetManager>();
    renderer_ = std::make_unique<Renderer>();
    scene_ = std::make_shared<Scene>();
}

App::~App() {

    renderer_.reset();
    time_.reset();
    input_.reset();
    window_.reset();
    instance_.reset();
}

void App::run() {
    running__ = true;

    std::signal(SIGINT, signal_exit);
    std::signal(SIGABRT, signal_exit);
    std::signal(SIGTERM, signal_exit);

    uint8_t loops = 0;

    while (running__) {
        time_->frame_start();
        window_->handle_events();

        // update

        loops = 0;
        while (time_->logic_update() && loops < MAX_FRAME_SKIP) {
            time_->logic_tick();
            // fixed_update
            loops++;
        }

        window_->begin_frame();
        // display

        do_gui();

        window_->draw_gui();
        window_->end_frame();

        time_->frame_end();
    }
}

void App::do_gui() {
    /* Main Menu */
    if (show_menu_bar_) gui_main_menu();

    /* Windows */
    if (show_window_inspector_) gui_window_inspector(&show_window_inspector_);
    if (show_window_hierarchy_) gui_window_hierarchy(&show_window_hierarchy_);
    if (show_window_assets_) gui_window_assets(&show_window_assets_);
    if (show_window_renderer_) gui_window_renderer(&show_window_renderer_);
    if (show_window_debug_) gui_window_debug(&show_window_debug_);
    if (show_window_logs_) gui_window_logs(&show_window_logs_);
    if (show_window_about_) gui_window_about(&show_window_about_);
    if (show_window_metrics_) ImGui::ShowMetricsWindow(&show_window_metrics_);
    if (show_window_demo_) ImGui::ShowDemoWindow(&show_window_demo_);
}

void App::gui_main_menu() {
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            if (ImGui::MenuItem("Quit", "Alt+F4")) {
                quit();
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Window"))
        {
            if (ImGui::MenuItem("Inspector", "Ctrl+1")) {
                show_window_inspector_ = true;
            }
            if (ImGui::MenuItem("Hierarchy", "Ctrl+2")) {
                show_window_hierarchy_ = true;
            }
            if (ImGui::MenuItem("Assets", "Ctrl+3")) {
                show_window_assets_ = true;
            }
            if (ImGui::MenuItem("Renderer", "Ctrl+4")) {
                show_window_renderer_ = true;
            }
            if (ImGui::MenuItem("Debug", "Ctrl+5")) {
                show_window_debug_ = true;
            }
            ImGui::Separator();
            if (ImGui::MenuItem("Logs", "Ctrl+~")) {
                show_window_logs_ = true;
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Help"))
        {
            if (ImGui::MenuItem("Metrics")) {
                show_window_metrics_ = true;
            }
            if (ImGui::MenuItem("ImGui Demo Window")) {
                show_window_demo_ = true;
            }
            ImGui::Separator();
            if (ImGui::MenuItem("About")) {
                show_window_about_ = true;
            }
            ImGui::EndMenu();
        }
    }
    ImGui::EndMainMenuBar();
}

void App::gui_window_inspector(bool* _p_open) {
    if (!ImGui::Begin("Inspector", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::End();
}

void App::gui_window_hierarchy(bool* _p_open) {
    if (!ImGui::Begin("Hierarchy", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::Text("Test field:");
    ImGui::Columns(3);
    ImGui::Separator();
    ImGui::Text("One"); ImGui::NextColumn();
    ImGui::Text("Two"); ImGui::NextColumn();
    ImGui::Text("Three"); ImGui::NextColumn();
    ImGui::Separator();
    ImGui::Text("Alpha"); ImGui::NextColumn();
    ImGui::Text("Beta"); ImGui::NextColumn();
    ImGui::Text("Gamma"); ImGui::NextColumn();
    ImGui::Columns(1);
    ImGui::End();
}

void App::gui_window_assets(bool* _p_open) {
    if (!ImGui::Begin("Assets", _p_open)) {
        ImGui::End();
        return;
    }

    asset_manager_->gui_inspector();

    ImGui::End();
}

void App::gui_window_renderer(bool* _p_open) {
    if (!ImGui::Begin("Renderer", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::End();
}

void App::gui_window_debug(bool* _p_open) {
    if (!ImGui::Begin("Debug", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::End();
}

void App::gui_window_logs(bool* _p_open) {
    if (!ImGui::Begin("Logs", _p_open)) {
        ImGui::End();
        return;
    }

    log_->gui_draw();

    ImGui::End();
}

void App::gui_window_about(bool* _p_open) {
    if (!ImGui::Begin("About", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::Text("Ember");
    ImGui::Separator();
    ImGui::Text("Version: %s", VERSION);
    ImGui::Text("Dear ImGui: %s", ImGui::GetVersion());
    ImGui::Separator();
    ImGui::Text("Ember is licensed under the MIT License.");

    ImGui::End();
}

void App::quit() {
    running__ = false;
}