/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/object.h>

#include <algorithm>

using namespace ember;

ObjectManager* ObjectManager::ref__ = nullptr;

ObjectManager::ObjectManager()
: next_(1) {
    ref__ = this;
}

ObjectManager::~ObjectManager() {
    ref__ = nullptr;
}

ember::id_t ObjectManager::assign() {
    if (ref__->objects_.empty()) {
        ref__->objects_.insert(1);
        ref__->next_ = 2;
        return 1;
    }

    auto next_itr = std::find(ref__->objects_.cbegin(), ref__->objects_.cend(), ref__->next_);
    if (next_itr != ref__->objects_.cend())
    {
        while (true)
        {
            if (next_itr == ref__->objects_.cend())
                break;
            if (*next_itr != ref__->next_)
                break;
            ref__->next_++;
            ++next_itr;
        }
    }

    id_t now = ref__->next_;
    ref__->objects_.insert(now);
    ref__->next_++;

    return now;
}

void ObjectManager::release(ember::id_t _id) {
    if (ref__->objects_.find(_id) != ref__->objects_.end()) {
        ref__->objects_.erase(_id);
        ref__->next_ = _id;
    }
}

void ObjectManager::release_all() {
    ref__->objects_.clear();
    ref__->next_ = 1;
}

size_t ObjectManager::count() {
    return ref__->objects_.size();
}
