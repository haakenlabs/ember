/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/scene.h>

#include <algorithm>

#include <ember/core/component.h>
#include <ember/core/entity.h>

using namespace ember;

Scene* Scene::ref__ = nullptr;

Scene::Scene()
: selected_node_(nullptr)
, loaded_(false)
, started_(false)
, dirty_(false) {
    ref__ = this;
    nodes_[0] = std::make_unique<node>(nullptr, 0, true);
}

Scene::~Scene() {
    ref__ = nullptr;
}

void Scene::load() {

}

void Scene::add_entity(entity_ptr _entity, id_t _parent) {
    if (has_entity(_entity->get_id())) {
        return;
    }
    if (!has_entity(_parent)) {
        return;
    }

    _entity->scene_ = shared_from_this();

    auto p = get_node(_parent);
    p->children_.push_back(_entity->get_id());

    nodes_[_entity->get_id()] = std::make_unique<node>(_entity, _parent, _entity->active_self_);

    update_caches();
}

void Scene::move_entity(id_t _id, id_t _parent) {
    if (_id == _parent) {
        return;
    }
    auto e = get_node(_id);
    if (!e) {
        return;
    }
    if (e->parent_ == _parent) {
        return;
    }

    if (!has_entity(_parent)) {
        return;
    }
    auto p = get_node(_parent);
    auto o = get_node(e->parent_);

    node_remove_child(o, _id);

    p->children_.push_back(_id);
    e->parent_ = _parent;

    update_caches();
}

void Scene::remove_entity(id_t _id) {
    auto items = dfs(_id, true);
    if (items.empty()) {
        return;
    }

    auto e = get_node(items[0]);
    if (auto p = get_node(e->parent_)) {
        node_remove_child(p, items[0]);
    }

    for (auto &item : items) {
        nodes_.erase(item);
    }

    update_caches();
}

void Scene::set_entity_active(id_t _id, bool _active) {
    auto e = get_node(_id);
    if (!e) {
        return;
    }

    update_caches();
}

bool Scene::has_entity(id_t _id) const {
    try {
        nodes_.at(_id);
    }
    catch(const std::out_of_range &_e) {
        return false;
    }

    return true;
}

bool Scene::entity_has_child(id_t _id, id_t _child) const {
    auto n = get_node(_id);
    if (!n) {
        return false;
    }

    for (auto &c : n->children_) {
        if (c == _child) {
            return true;
        }
    }

    return false;
}

entity_p Scene::entity_by_name(const std::string& _name) const {
    for (auto &n : nodes_) {
        if (n.second->entity_ && n.second->entity_->get_name() == _name) {
            return n.second->entity_.get();
        }
    }

    return nullptr;
}

entity_p Scene::entity_parent(id_t _id) const {
    auto n = get_node(_id);
    if (!n) {
        return nullptr;
    }

    if (!n->parent_) {
        return nullptr;
    }

    auto p = get_node(n->parent_);
    if (!p) {
        return nullptr;
    }

    return p->entity_.get();
}

entity_p Scene::entity_at_node(id_t _id) const {
    auto n = get_node(_id);
    if (!n) {
        return nullptr;
    }

    return n->entity_.get();
}

std::vector<entity_p> Scene::entity_children(id_t _id) const {
    std::vector<entity_p> children;

    auto n = get_node(_id);
    if (!n) {
        return children;
    }

    for (auto c : n->children_) {
        if (auto x = get_node(c)) {
            children.push_back(x->entity_.get());
        }
    }

    return children;
}

std::vector<entity_p> Scene::entity_all_children(id_t _id) const {
    auto n = get_node(_id);
    if (!n) {
        return std::vector<entity_p>{};
    }

    return node_all_child_entities(n);
}

std::vector<component_p> Scene::entity_components_in_parent(id_t _id) const {
    std::vector<component_p> components;

    auto n = get_node(_id);
    if (!n) {
        return components;
    }

    for (auto &p : node_all_parent_entities(n)) {
        for (auto &c : p->components_) {
            components.push_back(c.get());
        }
    }

    return components;
}

std::vector<component_p> Scene::entity_components_in_children(id_t _id) const {
    std::vector<component_p> components;

    auto n = get_node(_id);
    if (!n) {
        return components;
    }

    for (auto &p : node_all_child_entities(n)) {
        for (auto &c : p->components_) {
            components.push_back(c.get());
        }
    }

    return components;
}

void Scene::update() {
    if (dirty_) {
        update_caches();
    }

    if (!started_) {
        started_ = true;
        send_message(MessageStart);
    }

    send_message(MessageUpdate);
    send_message(MessageLateUpdate);
}

void Scene::fixed_update() {
    if (dirty_) {
        update_caches();
    }

    send_message(MessageFixedUpdate);
}

void Scene::display() {
    if (dirty_) {
        update_caches();
    }

    if (!r_cache_.empty()) {
//        renderer_->render(r_cache_.back().get(), d_cache_, l_cache_);
    }
}

void Scene::update_caches() {
    e_cache_.clear();
    c_cache_.clear();
    r_cache_.clear();
    d_cache_.clear();
    l_cache_.clear();

    update_tree(get_node(0), true);
    dirty_ = false;
}

void Scene::update_tree(node* _n, bool _active) {
    if (_n->entity_) {
        _active = _active && _n->entity_->active_self_;
        _n->entity_->active_hierarchy_ = _active;

        if (_active) {
            e_cache_.push_back(_n->entity_);
            for (auto &c : _n->entity_->components_) {
                c_cache_.push_back(c);

                if (auto v = std::dynamic_pointer_cast<Drawable>(c)) {
                    d_cache_.push_back(v);
                }
                if (auto v = std::dynamic_pointer_cast<Renderable>(c)) {
                    r_cache_.push_back(v);
                }
                if (auto v = std::dynamic_pointer_cast<Light>(c)) {
                    l_cache_.push_back(v);
                }
            }
        }
    }

    _n->active_ = _active;

    for (auto c : _n->children_) {
        update_tree(get_node(c), _active);
    }
}

void Scene::node_remove_child(node* _n, id_t _child) {
    auto itr = std::find(_n->children_.begin(), _n->children_.end(), _child);
    if (itr != _n->children_.end()) {
        _n->children_.erase(itr);
    }
}

void Scene::send_message(Message _m) {
    for (auto const &e : e_cache_) {
        e->send_message(_m);
    }
}

std::vector<id_t> Scene::dfs(id_t _id, bool _include_inactive) {
    auto n = get_node(_id);
    if (!n) {
        return std::vector<id_t>{};
    }

    if (!_include_inactive && !n->active_) {
        return std::vector<id_t>{};
    }

    auto ids = std::vector<id_t>{_id};

    for (auto c : n->children_) {
        auto x = dfs(c, _include_inactive);
        ids.insert(ids.end(), x.begin(), x.end());
    }

    return ids;
}

std::vector<entity_p> Scene::node_all_child_entities(node* _n) const {
    std::vector<entity_p> entities;

    for (auto &c : _n->children_) {
        if (auto x = get_node(c)) {
            auto other = node_all_child_entities(x);
            entities.insert(entities.end(), other.begin(), other.end());
        }
    }

    return entities;
}

std::vector<entity_p> Scene::node_all_parent_entities(node* _n) const {
    std::vector<entity_p> entities;

    if (!_n->parent_) {
        return entities;
    }

    auto p = get_node(_n->parent_);

    while (p) {
        entities.push_back(p->entity_.get());
        p = get_node(_n->parent_);
    }

    return entities;
}

Scene::node* Scene::get_node(id_t _id) const {
    try {
        return nodes_.at(_id).get();
    }
    catch(const std::out_of_range &_e) {
        return nullptr;
    }
}
