/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/entity.h>

#include <ember/core/scene.h>
#include <ember/core/transform.h>

using namespace ember;

Entity::Entity(std::string _name)
: active_self_(true)
, active_hierarchy_(true) {
    name_ = std::move(_name);
    class_name_ = "Entity";

    components_.push_back(std::make_shared<Transform>());
}

bool Entity::is_active() const {
    return active_self_ && active_hierarchy_;
}

bool Entity::is_active_self() const {
    return active_self_;
}

bool Entity::is_active_in_hierarchy() const {
    return active_hierarchy_;
}

void Entity::set_active(bool _active) {
    if (active_self_ != _active) {
        active_self_ = _active;

        if (scene_) {
            scene_->set_entity_active(id_, _active);
        }
    }
}

void Entity::set_transform(transform_ptr _transform) {
    components_[0] = _transform;
}

void Entity::add_component(component_ptr _component) {
    for (auto &c : components_) {
        if (c->get_id() == _component->get_id()) {
            return;
        }
    }

    _component->entity_ = shared_from_this();
    components_.push_back(_component);
}

entity_p Entity::get_parent() const {
    if (scene_) {
        return scene_->entity_parent(id_);
    }

    return nullptr;
}

scene_p Entity::get_scene() const {
    return scene_.get();
}

transform_p Entity::get_transform() const {
    return dynamic_cast<Transform*>(components_[0].get());
}

std::vector<component_p> Entity::_get_components_in_children() const {
    if (scene_) {
        return scene_->entity_components_in_children(id_);
    }

    return std::vector<component_p>{};
}

std::vector<component_p> Entity::_get_components_in_parent() const {
    if (scene_) {
        return scene_->entity_components_in_parent(id_);
    }

    return std::vector<component_p>{};
}

void Entity::gui_inspector() {

}

void Entity::send_message(Message _m) {
    if (!active_hierarchy_) {
        return;
    }

    for (auto const &c : components_) {
        switch (_m) {
            case MessageStart:
                if (!c->entity_) {
                    c->entity_ = shared_from_this();
                }
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->start();
                }
                break;
            case MessageActivate:
            case MessageAwake:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->awake();
                }
                break;
            case MessageUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->update();
                }
                break;
            case MessageLateUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->late_update();
                }
                break;
            case MessageFixedUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->fixed_update();
                }
                break;
            case MessageTransform:
                c->on_transform_changed();
                break;
            default:
                break;
        }
    }
}
