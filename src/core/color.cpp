/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/color.h>

#include <stdexcept>

#include <imgui/imgui.h>

using namespace ember;

const Color Color::white = Color(1.0f);
const Color Color::black = Color(0.0f);
const Color Color::red = Color(1.0f, 0.0f, 0.0f);
const Color Color::green = Color(0.0f, 1.0f, 0.0f);
const Color Color::blue = Color(0.0f, 0.0f, 1.0f);

Color::Color(float _r, float _g, float _b)
    : Color(_r, _g, _b, 1.0f) {}

Color::Color(float _r, float _g, float _b, float _a) {
    color_[0] = _r;
    color_[1] = _g;
    color_[2] = _b;
    color_[3] = _a;
}

Color::Color(const std::string& _str) {
}

Color::Color(float _v)
    : Color(_v, _v, _v) {}

Color::Color(glm::vec3 _v)
    : Color(_v.r, _v.g, _v.b) {}

Color::Color(glm::vec4 _v)
    : Color(_v.r, _v.g, _v.b, _v.a) {}

float& Color::operator[](int _i) {
    return color_[_i];
}

Color Color::operator-(Color _b) {
    return Color(
        glm::clamp(color_[0] - _b[0], 0.0f, 1.0f),
        glm::clamp(color_[1] - _b[1], 0.0f, 1.0f),
        glm::clamp(color_[2] - _b[2], 0.0f, 1.0f),
        glm::clamp(color_[3] - _b[3], 0.0f, 1.0f));
}

Color Color::operator+(Color _b) {
    return Color(
        glm::clamp(color_[0] + _b[0], 0.0f, 1.0f),
        glm::clamp(color_[1] + _b[1], 0.0f, 1.0f),
        glm::clamp(color_[2] + _b[2], 0.0f, 1.0f),
        glm::clamp(color_[3] + _b[3], 0.0f, 1.0f));
}

Color Color::operator*(Color _b) {
    return Color(
        glm::clamp(color_[0] * _b[0], 0.0f, 1.0f),
        glm::clamp(color_[1] * _b[1], 0.0f, 1.0f),
        glm::clamp(color_[2] * _b[2], 0.0f, 1.0f),
        glm::clamp(color_[3] * _b[3], 0.0f, 1.0f));
}

Color Color::operator*(float _b) {
    return Color(
        glm::clamp(color_[0] * _b, 0.0f, 1.0f),
        glm::clamp(color_[1] * _b, 0.0f, 1.0f),
        glm::clamp(color_[2] * _b, 0.0f, 1.0f),
        glm::clamp(color_[3] * _b, 0.0f, 1.0f));
}

Color Color::operator/(float _b) {
    return Color(
        glm::clamp(color_[0] / _b, 0.0f, 1.0f),
        glm::clamp(color_[1] / _b, 0.0f, 1.0f),
        glm::clamp(color_[2] / _b, 0.0f, 1.0f),
        glm::clamp(color_[3] / _b, 0.0f, 1.0f));
}

void Color::gui_inspector() {
    ImGui::ColorPicker4("Color", color_);
}

Color Color::gamma() const {
    auto v3 = as_vec3();
    v3 = glm::pow(v3, glm::vec3(1.0f/2.2f));
    return Color(v3);
}

Color Color::grayscale() const {
    return Color((color_[0]+color_[1]+color_[2])/3.0f);
}

Color Color::linear() const {
    return Color::white;
}
float Color::max_color_comonent() const {
    return glm::max(color_[0], glm::max(color_[1], color_[2]));
}

glm::vec3 Color::as_vec3() const {
    return glm::vec3(color_[0], color_[1], color_[2]);
}

glm::vec4 Color::as_vec4() const {
    return glm::vec4(color_[0], color_[1], color_[2], color_[3]);
}

Color Color::hsv_to_rgb(float _h, float _s, float _v) {
    return Color::white;
}

Color Color::hsv_to_rgb(float _h, float _s, float _v, bool _hdr) {
    return Color::white;
}

Color Color::lerp(Color _a, Color _b, float _t) {
    return Color::white;
}

Color Color::lerp_unclamped(Color _a, Color _b, float _t) {
    return Color::white;
}

Color Color::rgb_to_hsv(Color _color, float& _h, float& _s, float& _v) {
    return Color::white;
}
