/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/core/object.h>

#include <imgui/imgui.h>

using namespace ember;

Object::Object()
: id_(ObjectManager::assign())
, class_name_("Object")
, name_("Object") {}

void Object::set_name(const std::string &_name) {
    if (!_name.empty()) {
        name_ = _name;
    }
}

ember::id_t Object::get_id() const {
    return id_;
}

std::string Object::get_name() const {
    return name_;
}

std::string Object::get_class_name() const {
    return class_name_;
}

void Object::gui_inspector() {
    ImGui::Text("Name: %s", name_.c_str());
    ImGui::Text("Class Name: %s", name_.c_str());
    ImGui::Text("ID: %08X", id_);
}

