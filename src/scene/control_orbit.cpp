/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/scene/control_orbit.h>

#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui/imgui.h>

#include <ember/core/entity.h>
#include <ember/core/input.h>
#include <ember/core/transform.h>
#include <ember/core/time.h>
#include <ember/scene/camera.h>

using namespace ember;

static glm::vec3 spherical_to_cartesian(float _radial, float _theta, float _phi) {
    auto st = glm::sin(_theta);
    auto ct = glm::cos(_theta);
    auto sp = glm::sin(_phi);
    auto cp = glm::cos(_phi);

    return glm::vec3(_radial * sp * st, _radial * cp, _radial * sp * ct);
}

ControlOrbit::ControlOrbit()
: target_(nullptr)
, radial_(glm::vec3(4, 0.1f, 0))
, phi_(glm::vec3(glm::half_pi<float>(), 0.1f, 0))
, theta_(glm::vec3(0, 0.1f, 0))
, mouse_start_(glm::vec2(0))
, mouse_last_(glm::vec2(0))
, mouse_delta_(glm::vec2(0))
, mouse_drag_(false)
, mouse_down_(false) {
    class_name_ = "ControlOrbit";
    name_ = "ControlOrbit";

    radial_.z = radial_.x;
    phi_.z = phi_.x;
    theta_.z = theta_.x;
}

void ControlOrbit::start() {
    if (auto c = entity_->get_component<Camera>()) {
        c->set_view_locked(true);
    }
    move();
}

void ControlOrbit::gui_inspector() {
    ImGui::InputFloat3("Radial", glm::value_ptr(radial_));
    ImGui::InputFloat3("Phi", glm::value_ptr(phi_));
    ImGui::InputFloat3("Theta", glm::value_ptr(theta_));
    ImGui::InputFloat2("Mouse Start", glm::value_ptr(mouse_start_));
    ImGui::InputFloat2("Mouse Last", glm::value_ptr(mouse_last_));
    ImGui::InputFloat2("Mouse Delta", glm::value_ptr(mouse_delta_));
    ImGui::Text("Mouse Drag: %s", mouse_drag_ ? "Yes" : "No");
    ImGui::Text("Mouse Down: %s", mouse_down_ ? "Yes" : "No");
    if (ImGui::Button("Reset View")) {
        radial_.x = 4;
        phi_.x = glm::half_pi<float>();
        theta_.x = 0;

        move();
        return;
    }
}

void ControlOrbit::late_update() {
    bool changed = false;
    float dx = 0;
    float dy = 0;

    if (Input::key_down(GLFW_KEY_R)) {
        radial_.x = 4;
        phi_.x = glm::half_pi<float>();
        theta_.x = 0;

        move();
        return;
    }

    if (Input::mouse_wheel()) {
        radial_.x -= Input::mouse_wheel_y() * 0.25f;
        if (radial_.x < 0.1) {
            radial_.x = 0.1;
        }
    }

    if (Input::mouse_down(GLFW_MOUSE_BUTTON_LEFT)) {
        mouse_down_ = true;
    }

    if (Input::mouse_moved() && mouse_down_) {
        if (!mouse_drag_) {
            mouse_drag_ = true;
            mouse_start_ = Input::mouse_position();
            mouse_last_ = Input::mouse_position();
        }
    }

    if (Input::mouse_up(GLFW_MOUSE_BUTTON_LEFT)) {
        mouse_down_ = false;
        mouse_drag_ = false;
    }

    if (mouse_drag_ && Input::mouse_moved()) {
        mouse_delta_ = glm::vec2(Input::mouse_position()) - mouse_last_;
        mouse_last_ = Input::mouse_position();

        dx = (mouse_delta_.x * 0.25f) * float(Time::get_delta());
        dy = (-mouse_delta_.y * 0.25f) * float(Time::get_delta());

        auto tmpPhi = phi_.x;

        theta_.x -= dx;
        tmpPhi += dy;

        if (tmpPhi > 0 && tmpPhi < glm::pi<float>()) {
            phi_.x = tmpPhi;
        }

        changed = true;
    }

    if (radial_.x != radial_.z) {
        radial_.z = glm::mix(radial_.z, radial_.x, radial_.y);

        if (glm::abs(radial_.x - radial_.z) < 0.001) {
            radial_.z = radial_.x;
        }

        changed = true;
    }

    if (phi_.x != phi_.z) {
        phi_.z = glm::mix(phi_.z, phi_.x, phi_.y);

        if (glm::abs(phi_.x - phi_.z) < 0.001) {
            phi_.z = phi_.x;
        }

        changed = true;
    }

    if (theta_.x != theta_.z) {
        theta_.z = glm::mix(theta_.z, theta_.x, theta_.y);

        if (glm::abs(theta_.x - theta_.z) < 0.001) {
            if (theta_.x > glm::two_pi<float>()) {
                while (theta_.x > glm::two_pi<float>()) {
                    theta_.x -= glm::two_pi<float>();
                }
            } else if (theta_.x < -glm::two_pi<float>()) {
                while (theta_.x < -glm::two_pi<float>()) {
                    theta_.x += glm::two_pi<float>();
                }
            }

            theta_.z = theta_.x;
        }

        changed = true;
    }

    if (changed) {
        move();
    }
}

void ControlOrbit::move() {
    auto position = glm::vec3(0);
    if (target_) {
        position = target_->get_transform()->get_position();
    }

    get_transform()->set_position(spherical_to_cartesian(radial_.z, theta_.z, phi_.z), false);

    auto view_matrix = glm::lookAt(get_transform()->get_position(), position, glm::vec3(0, 1, 0));
    get_transform()->set_rotation(glm::quat_cast(view_matrix), true);

    if (auto c = entity_->get_component<Camera>()) {
        c->set_view_matrix((view_matrix));
    }
}
