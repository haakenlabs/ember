/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/scene/mesh_renderer.h>

#include <imgui/imgui.h>

#include <ember/core/log.h>
#include <ember/core/scene.h>
#include <ember/core/transform.h>
#include <ember/gfx/material.h>
#include <ember/gfx/mesh.h>
#include <ember/gfx/texture.h>

using namespace ember;

MeshRenderer::MeshRenderer(mesh_ptr _mesh, material_ptr _material)
: mesh_(std::move(_mesh))
, material_(std::move(_material)) {
    class_name_ = "MeshRenderer";
    name_ = "MeshRenderer";
}

void MeshRenderer::set_mesh(mesh_ptr _mesh) {
    if (!_mesh) {
        LOG_ERROR("MeshRenderer mesh cannot be null");
        return;
    }

    mesh_ = std::move(_mesh);
}

void MeshRenderer::set_material(material_ptr _material) {
    if (!_material) {
        LOG_ERROR("MeshRenderer material cannot be null");
        return;
    }

    material_ = std::move(_material);
}

void MeshRenderer::draw(renderable_p _renderable) {
    material_->bind();

//    if (is_deferrable()) {
//        if (get_scene()->get_renderer()->get_active_mode() == RenderModeForward) {
//            material_->get_shader()->set_subroutine(ShaderComponentFragment, "forward_pass");
//        } else {
//            material_->get_shader()->set_subroutine(ShaderComponentFragment, "deferred_pass_geometry");
//        }
//    }

    draw_shader(_renderable, material_->get_shader());

    material_->unbind();
}

void MeshRenderer::draw_shader(renderable_p _renderable, shader_p _shader) {
    _shader->set_uniform("v_model_matrix", get_transform()->get_active_matrix());
    _shader->set_uniform("v_view_matrix", _renderable->get_view_matrix());
    _shader->set_uniform("v_projection_matrix", _renderable->get_projection_matrix());
    _shader->set_uniform("v_normal_matrix", _renderable->get_normal_matrix());
    _shader->set_uniform("f_camera", _renderable->get_position());

    mesh_->bind();
    mesh_->draw();
    mesh_->unbind();
}

bool MeshRenderer::is_deferrable() const {
    return material_->get_shader()->supports_deferred();
}

void MeshRenderer::gui_inspector() {
    ImGui::Text("Mesh: %s", mesh_->get_name().c_str());
    if (ImGui::CollapsingHeader("Material")) {
        material_->gui_inspector();
    }
}
