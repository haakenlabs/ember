/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/scene/light.h>

#include <imgui/imgui.h>

using namespace ember;

const char* light_type_items[] = {"Directional", "Point", "Area", "Spot"};

Light::Light()
: Light(LightTypeDirectional, Color::white) {}

Light::Light(LightType _type)
: Light(_type, Color::white) {}

Light::Light(LightType _type, Color _color)
: type_(_type)
, color_(_color) {
    class_name_ = "Light";
    name_ = "Light";
}

void Light::gui_inspector() {
    int type_index = (int)type_;
    ImGui::Combo("Type", &type_index, light_type_items, IM_ARRAYSIZE(light_type_items));
    ImGui::Separator();
    ImGui::DragFloat("Bounce Intensity", &bounce_intensity_);
    ImGui::DragFloat("Color Temperature", &bounce_intensity_);
    ImGui::DragFloat("Intensity", &bounce_intensity_);
    ImGui::DragFloat("Range", &bounce_intensity_);
    ImGui::DragFloat("Spot Angle", &bounce_intensity_);
    ImGui::Separator();
    color_.gui_inspector();

    if (type_index != (int)type_) {
        type_ = (LightType)type_index;
    }
}

void Light::set_color(Color _color) {
    color_ = _color;
}

void Light::set_area_size(glm::vec2 _area_size) {
    area_size_ = _area_size;
}

void Light::set_type(LightType _type) {
    type_ = _type;
}

void Light::set_bounce_intensity(float _bounce_intensity) {
    bounce_intensity_ = _bounce_intensity;
}

void Light::set_color_temperature(float _color_temperature) {
    color_temperature_ = _color_temperature;
}

void Light::set_intensity(float _intensity) {
    intensity_ = _intensity;
}

void Light::set_range(float _range) {
    range_ = _range;
}

void Light::set_spot_angle(float _spot_angle) {
    spot_angle_ = _spot_angle;
}

Color Light::get_color() const {
    return color_;
}

glm::vec2 Light::get_area_size() const {
    return area_size_;
}

LightType Light::get_type() const {
    return type_;
}

float Light::get_bounce_intensity() const {
    return bounce_intensity_;
}

float Light::get_color_temperature() const {
    return color_temperature_;
}

float Light::get_intensity() const {
    return intensity_;
}

float Light::get_range() const {
    return range_;
}

float Light::get_spot_angle() const {
    return spot_angle_;
}

const char* ember::light_type_to_str(LightType _type) {
    switch (_type) {
        case LightTypeDirectional:
            return "Directional";
        case LightTypePoint:
            return "Point";
        case LightTypeArea:
            return "Area";
        case LightTypeSpot:
            return "Spot";
        default:
            return "<Unknown>";
    }
}
