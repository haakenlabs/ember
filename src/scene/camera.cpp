/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ember/scene/camera.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <ember/core/transform.h>
#include <ember/core/window.h>
#include <ember/core/input.h>

using namespace ember;

Camera::Camera()
: view_matrix_(glm::mat4(1))
, projection_matrix_(glm::mat4(0))
, normal_matrix_(glm::mat3(0))
, fov_(glm::radians(75.0f))
, near_clip_(0.1f)
, far_clip_(10000.0f)
, orthographic_(false)
, locked_view_(false) {
    class_name_ = "Camera";
    name_ = "Camera";
}

void Camera::update_matrices() {
    if (orthographic_) {
        auto res = Window::get_resolution();
        projection_matrix_ = glm::ortho(0, viewport_.x, viewport_.y, 0);
    } else {
        float aspect = viewport_.y ? (float)viewport_.x / (float)viewport_.y : 0.f;
        projection_matrix_ = glm::perspective(fov_, aspect, near_clip_, far_clip_);
    }

    if (!locked_view_) {
        if (auto const &t = get_transform()) {
            auto eye = t->get_position();
            view_matrix_ = glm::lookAt(eye, eye-glm::vec3(0, 0, 1), glm::vec3(0, 1, 0));
        }
    }
}

void Camera::on_transform_changed() {
    if (!locked_view_) {
        auto eye = get_transform()->get_position();
        view_matrix_ = glm::lookAt(eye, eye-glm::vec3(0, 0, 1), glm::vec3(0, 1, 0));
    }
}

void Camera::set_view_locked(bool _lock) {
    locked_view_ = _lock;
}

void Camera::set_viewport(glm::ivec2 _viewport) {
    viewport_ = _viewport;
    update_matrices();
}

glm::ivec2 Camera::get_viewport() const {
    return viewport_;
}

void Camera::gui_inspector() {
    ImGui::Text("Projection: %s", orthographic_ ? "Orthographic" : "Perspective");
    ImGui::Text("Near Clip: %f", near_clip_);
    ImGui::Text("Far Clip: %f", far_clip_);
    ImGui::Text("FOV: %f", glm::degrees(fov_));
}

void Camera::set_near_clip(float _near_clip) {
    near_clip_ = _near_clip;
}

void Camera::set_far_clip(float _far_clip) {
    far_clip_ = _far_clip;
}

void Camera::set_fov(float _fov) {
    fov_ = _fov;
}

void Camera::set_view_matrix(glm::mat4 _view_matrix) {
    view_matrix_ = _view_matrix;
}

float Camera::get_near_clip() const {
    return near_clip_;
}

float Camera::get_far_clip() const {
    return far_clip_;
}

glm::vec3 Camera::get_position() const {
    if (auto t = get_transform()) {
        return t->get_position();
    }

    return glm::vec3(0);
}

glm::mat3 Camera::get_normal_matrix() const {
    return normal_matrix_;
}

glm::mat4 Camera::get_view_matrix() const {
    return view_matrix_;
}

glm::mat4 Camera::get_projection_matrix() const {
    return projection_matrix_;
}

