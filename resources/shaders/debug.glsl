#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 vo_position;
out vec3 vo_normal;
out vec2 vo_texture;

void main()
{
    vo_texture = uv;
    vo_normal = normal;
    vo_position = vertex;

    gl_Position = vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
subroutine void RenderPassType();
subroutine uniform RenderPassType RenderPass;

in vec3 vo_position;
in vec3 vo_normal;
in vec2 vo_texture;

layout(location = 0) out vec4 fo_attachment0;
layout(location = 1) out uvec4 fo_attachment1;

layout(binding = 0) uniform sampler2D f_attachment0;
layout(binding = 1) uniform usampler2D f_attachment1;
layout(binding = 2) uniform sampler2D f_depth;

uniform mat4 f_view_matrix;
uniform mat4 f_projection_matrix;

vec3 get_position(vec4 data)
{
    return texture(f_attachment0, vo_texture).xyz;
}

vec3 get_normal()
{
    vec3 N = texture(f_attachment1, vo_texture).rgb;

    normal.xy = unpackHalf2x16(N.x);
    normal.z = unpackHalf2x16(N.y).x;

    return normal;
}

vec3 get_albedo()
{
    return unpackUnorm4x8(texture(f_attachment1, vo_texture).z).rgb;
}

vec3 get_reflection(vec3 dir)
{
    return texture(f_environment_map, dir).rgb;
}

float get_metallic()
{
    return unpackHalf2x16(texture(f_attachment1, vo_texture).w).y;
}

float get_roughness() {
    return unpackHalf2x16(texture(f_attachment1, vo_texture).w).x;
}

float get_ao() {
    return unpackHalf2x16(texture(f_attachment1, vo_texture).y).y;
}

subroutine(RenderPassType)
void debug_visualize_position() {
    fo_color = vec4(get_position(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_normal() {
    fo_color = vec4(get_normal(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_depth() {
    fo_color = vec4(vec3(texture(f_depth, vo_texture).r), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_albedo() {
    fo_color = vec4(get_albedo(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_metallic() {
    fo_color = vec4(vec3(get_metallic(texture(f_attachment1))), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_roughness() {
    fo_color = vec4(vec3(get_roughness()), 1.0);
}

void main()
{
    RenderPass();
}

#endif
