#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 vo_position;
out vec3 vo_normal;
out vec3 vo_eye;
out vec3 vo_ws_position;
out vec3 vo_ws_normal;
out vec2 vo_texture;

uniform mat4 v_mvp_matrix;
uniform mat4 v_projection_matrix;
uniform mat4 v_view_matrix;
uniform mat4 v_model_matrix;
uniform mat3 v_normal_matrix;

void main()
{
    vo_texture = uv;
    vo_normal = normal;
    vo_position = vertex;
    vo_ws_position = vec3(v_model_matrix * vec4(vertex, 1.0));
    vo_ws_normal = vec3(v_model_matrix * vec4(normal, 1.0));

    gl_Position = v_projection_matrix * v_view_matrix * v_model_matrix * vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
subroutine void RenderPassType();
subroutine uniform RenderPassType RenderPass;

in vec3 vo_position;
in vec3 vo_normal;
in vec3 vo_eye;
in vec3 vo_ws_position;
in vec3 vo_ws_normal;
in vec2 vo_texture;

layout(location = 0) out vec4 fo_attachment0;
layout(location = 1) out uvec4 fo_attachment1;

layout(binding = 0) uniform sampler2D f_attachment0;
layout(binding = 1) uniform usampler2D f_attachment1;
layout(binding = 2) uniform sampler2D f_depth;

/* Uniform bindings for PBR lighting */
layout(binding = 3) uniform samplerCube f_environment_map;
layout(binding = 4) uniform samplerCube f_irradiance_map;
layout(binding = 5) uniform samplerCube f_specular_map;
layout(binding = 6) uniform sampler2D f_brdf_lut;

/* Uniform bindings for Materials */
layout(binding = 7) uniform sampler2D f_albedo_map;
layout(binding = 8) uniform sampler2D f_metallic_map;
layout(binding = 9) uniform sampler2D f_normal_map;
layout(binding = 10) uniform sampler2D f_roughness_map;
layout(binding = 11) uniform sampler2D f_ao_map;

uniform vec3 f_camera;
uniform vec3 f_albedo;
uniform float f_roughness;
uniform float f_metallic;
uniform bool u_use_albedo_map = false;
uniform bool u_use_metallic_map = false;
uniform bool u_use_roughness_map = false;
uniform bool u_use_normal_map = false;

uniform mat4 f_view_matrix;
uniform mat4 f_projection_matrix;

uniform vec3 lightPositions[4];
uniform vec3 lightColors[4];

#define PI   3.1415926535897932384626433832795
#define PI2  6.2831853071795864769252867665590

const float MAX_REFLECTION_LOD = 4.0;

vec3 get_position(vec4 data)
{
    return data.xyz;
}

vec3 get_normal_from_map() {
    vec3 tangentNormal = texture(f_normal_map, vo_texture).xyz * 2.0 - 1.0;

    vec3 Q1  = dFdx(vo_ws_position);
    vec3 Q2  = dFdy(vo_ws_position);
    vec2 st1 = dFdx(vo_texture);
    vec2 st2 = dFdy(vo_texture);

    vec3 N   = normalize(vo_ws_normal);
    vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B  = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

vec3 get_normal(uvec4 data)
{
    vec3 normal;

    normal.xy = unpackHalf2x16(data.x);
    normal.z = unpackHalf2x16(data.y).x;

    return normal;
}

vec3 get_albedo(uvec4 data)
{
    return unpackUnorm4x8(data.z).rgb;
}

vec3 get_reflection(vec3 dir)
{
    return texture(f_environment_map, dir).rgb;
}

float get_metallic(uvec4 data)
{
    return unpackHalf2x16(data.w).y;
}

float get_roughness(uvec4 data) {
    return unpackHalf2x16(data.w).x;
}

float get_ao(uvec4 data) {
    return unpackHalf2x16(data.y).y;
}

float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

subroutine(RenderPassType)
void forward_pass()
{
    vec3 V = normalize(f_camera - vo_ws_position);
    vec3 N = vo_ws_normal;
    vec3 L = normalize(-reflect(V, N));
    vec3 C = texture(f_environment_map, L).rgb;

    fo_attachment0 = vec4(C, 1.0);
}

subroutine(RenderPassType)
void deferred_pass_geometry()
{
    fo_attachment0 = vec4(vo_ws_position, 1.0);

    vec3 albedo = texture(f_albedo_map, vo_texture).rgb;
    float roughness = texture(f_roughness_map, vo_texture).r;
    float metallic = texture(f_metallic_map, vo_texture).r;
    float ao = texture(f_ao_map, vo_texture).r;

    vec3 N = get_normal_from_map();

    fo_attachment1.x = packHalf2x16(N.xy);
    fo_attachment1.y = packHalf2x16(vec2(N.z, ao));
    fo_attachment1.z = packUnorm4x8(vec4(albedo, 1.0));
    fo_attachment1.w = packHalf2x16(vec2(roughness, metallic));
}

subroutine(RenderPassType)
void deferred_pass_ambient()
{
    float depth = texture(f_depth, vo_texture).r;
    if (depth == 1.0) {
        discard;
    }

    vec4 data0 = texture(f_attachment0, vo_texture);
    uvec4 data1 = texture(f_attachment1, vo_texture);

    vec3 albedo = pow(get_albedo(data1).rgb, vec3(2.2));
    float metallic = get_metallic(data1);
    float roughness = get_roughness(data1);
    float ao = get_ao(data1);
    ao = 1.0;

    vec3 N = get_normal(data1);
    vec3 P = get_position(data0);
    vec3 V = normalize(f_camera - P);
    vec3 R = reflect(-V, N);

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
//    for(int i = 0; i < 4; ++i)
//    {
//        // calculate per-light radiance
//        vec3 L = normalize(lightPositions[i] - P);
//        vec3 H = normalize(V + L);
//        float distance = length(lightPositions[i] - P);
//        float attenuation = 1.0 / (distance * distance);
//        vec3 radiance = lightColors[i] * attenuation;
//
//        // Cook-Torrance BRDF
//        float NDF = DistributionGGX(N, H, roughness);
//        float G = GeometrySmith(N, V, L, roughness);
//        vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);
//
//        vec3 nominator = NDF * G * F;
//        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
//        vec3 specular = nominator / denominator;
//
//        // kS is equal to Fresnel
//        vec3 kS = F;
//        // for energy conservation, the diffuse and specular light can't
//        // be above 1.0 (unless the surface emits light); to preserve this
//        // relationship the diffuse component (kD) should equal 1.0 - kS.
//        vec3 kD = vec3(1.0) - kS;
//        // multiply kD by the inverse metalness such that only non-metals
//        // have diffuse lighting, or a linear blend if partly metal (pure metals
//        // have no diffuse light).
//        kD *= 1.0 - metallic;
//
//        // scale light by NdotL
//        float NdotL = max(dot(N, L), 0.0);
//
//        // add to outgoing radiance Lo
//        Lo += (kD * albedo / PI + specular) * radiance * NdotL;  // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
//    }

    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);

    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - metallic;

    vec3 irradiance = texture(f_irradiance_map, N).rgb;
    vec3 diffuse = irradiance * albedo;

    vec3 prefiltered_color = textureLod(f_specular_map, R, roughness * MAX_REFLECTION_LOD).rgb;
    vec2 brdf = texture(f_brdf_lut, vec2(max(dot(N, V), 0.0), roughness)).rg;
    vec3 specular = prefiltered_color * (F * brdf.x + brdf.y);

    vec3 ambient = (kD * diffuse + specular) * ao;

    vec3 color = ambient + Lo;

    fo_attachment0 = vec4(color, 1.0);
}

subroutine(RenderPassType)
void debug_visualize_position() {
    fo_color = vec4(get_position(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_normal() {
    fo_color = vec4(get_normal(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_depth() {
    fo_color = vec4(vec3(texture(f_depth, vo_texture).r), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_albedo() {
    fo_color = vec4(get_albedo(), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_metallic() {
    fo_color = vec4(vec3(get_metallic()), 1.0);
}

subroutine(RenderPassType)
void debug_visualize_roughness() {
    fo_color = vec4(vec3(get_roughness()), 1.0);
}

void main()
{
    RenderPass();
}

#endif
