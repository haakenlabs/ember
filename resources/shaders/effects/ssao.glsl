#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec2 vo_texture;

void main()
{
    vo_texture = uv;

    gl_Position = vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
in vec2 vo_texture;

out vec4 fo_color;

uniform int f_kernel_size = 64;
uniform float f_radius = 0.5;
uniform float f_bias = 0.025;
uniform vec2 f_resolution;
uniform vec3 f_samples[64];
uniform mat4 f_view_matrix;
uniform mat4 f_projection_matrix;

layout(binding = 0) uniform sampler2D f_attachment0;
layout(binding = 1) uniform usampler2D f_attachment1;
layout(binding = 2) uniform sampler2D f_depth;
layout(binding = 3) uniform sampler2D f_noise;

vec3 get_position(vec4 data)
{
    return data.xyz;
}

vec3 get_normal(uvec4 data)
{
    vec3 normal;

    normal.xy = unpackHalf2x16(data.x);
    normal.z = unpackHalf2x16(data.y).x;

    return normal;
}

void main()
{
    vec2 noise_scale = f_resolution / 4.0;

    vec4 data0 = texture(f_attachment0, vo_texture);
    uvec4 data1 = texture(f_attachment1, vo_texture);

    vec3 P = get_position(data0);
    vec3 N = normalize(get_normal(data1));
    vec3 R = normalize(texture(f_noise, vo_texture * noise_scale).xyz);

    vec3 tangent = normalize(R - N * dot(R, N));
    vec3 bitangent = cross(N, tangent);
    mat3 TBN = mat3(tangent, bitangent, N);

    float occlusion = 0.0;
    for (int i = 0; i < f_kernel_size; i++) {
        vec3 S = TBN * f_samples[i];
        S = P + S * f_radius;

        vec4 offset = vec4(S, 1.0);
        offset = f_projection_matrix * f_view_matrix * offset;
        offset.xyz /= offset.w;
        offset.xyz = offset.xyz * 0.5 + 0.5;

        float depth = texture(f_attachment0, offset.xy).z;

        float range_check = smoothstep(0.0, 1.0, f_radius / abs(P.z - depth));
        occlusion += (depth >= S.z + f_bias ? 1.0 : 0.0) * range_check;
    }

    occlusion = 1.0 - (occlusion / f_kernel_size);

    fo_color = vec4(vec3(occlusion), 1.0);
}

#endif
